# **Introduccion a SQL con MySQL - Manipule y consulte datos**

## **Instalando y configurando MySQL**

### **Un poco más sobre SQL**

El acrónimo CRUD hace alusión a los estándares utilizados por el lenguaje SQL.

¿Qué quiere decir cada letra de este acrónimo?

Rta.

CREATE, READ, UPDATE y DELETE. Crear, leer, actualizar y eliminar son las funciones nativas de SQL.

### **Un poco más sobre MySQL**

Hacia finales de la primera década del 2000, hubo una serie de negociaciones que involucraron la adquisición de MySQL.

¿Qué Corporación es propietaria de MySQL actualmente?

Rta.

ORACLE compró a Sun Microsystems.

---

## **Manipulando la base de datos**

### **Localización de la tabla**

¿Dónde es creada la tabla dentro de MySQL?

Rta.

En la base de datos.

### **Componentes de las tablas**

Halla la alternativa falsa.

En una tabla tenemos:

```txt
a) Views, claves primarias, campos, registros.
b) En una tabla tenemos campos, registros, claves primarias y externas.
c) Claves primarias, claves externas, índices.
```

Rta.

a. Las views no hacen parte de una tabla. Luego, esta alternativa es falsa, por eso debe ser escogida conforme al enunciado de la pregunta.

### **Agrupando las tablas**

¿Cómo podemos agrupar las tablas?

- En Stored Procedures. ¡Alternativa incorrecta! En los Stored Procedures, hacemos programas para manipular datos de la base.
- En esquemas(Schema). ¡Alternativa correcta! Las tablas pueden estar contenidas en un esquema.
- En Views. ¡Alternativa incorrecta! Las views no son grupos de tablas, sino consultas que pueden, o no, contener varias tablas.

Rta.

En esquemas(Schema).

### **Comando para crear una base de datos**

¿Cuál es el comando utilizado para crear una base de datos?

Rta.

CREATE

### **Creando una base de datos**

¿Cómo podemos crear una base de datos en MySQL Workbench?

Rta.

A través de comandos SQL y asistentes.

### **Eliminación de una base de datos**

¿Cuál es el comando para borrar una base de datos?

Rta.

DROP

---

## **Administrando las tablas de la base de datos**

### **Tipo fecha**

Supongamos que yo tuviera un sistema de datos de una multinacional que controla llamadas de soporte. Una llamada es abierta en Japón y resuelta por el equipo de Alemania. ¿Qué tipo de campo fecha es el mejor para que podamos hacer cálculos correctos para obtener el tiempo de respuesta de los llamados?

Rta.

TIMESTAMP. Hay que tener en cuenta la zona horaria correspondiente a la fecha para tratar fechas en diferentes partes del mundo.

### **Mejor tipo de variable**

La empresa para la cual trabajas como DBA, te pidió que incluyeras un nuevo campo en la tabla de productos para colocar el valor en USD de los descuentos que serán aplicados a los mejores clientes. El valor máximo del descuento es de USD 900.00, y lógicamente habrá casos en los cuales los clientes recibirán un descuento con valores incluyendo los centavos.

¿Qué tipo de variable será la mejor opción para este campo?

Rta.

- DECIMAL(5,2) ¡Alternativa correcta! Esta es la mejor opción a la hora de mantener un valor exacto con los centavos, incluyendo los ceros a la derecha de la coma.
- FLOAT(5,2) ¡Alternativa incorrecta! Recuerda que FLOAT es coma flotante, y que su valor siempre tendrá la tendencia a redondear la cifra. (Si tienes 10 centavos, va a marcar únicamente 1, y el 0 lo va a omitir).

---

## **Mantenimiento de los datos en las tablas**

### **Incluyendo el primer vendedor**

Vamos a crear un vendedor en la tabla de vendedores. La información es la siguiente:

Matrícula: 00233
Nombre: Joan Geraldo de la Fonseca
Comisión: 10%

Digite el nombre de inclusión.

Rta.

```sql
INSERT INTO TABLA_DE_VENDEDORES
(MATRICULA, NOMBRE, PORCENTAJE_COMISION)
VALUES
('00233', 'Joan Geraldo de la Fonseca', 0.10);
```

### **Incluyendo dos vendedores más**

Vamos a crear dos vendedores más, en el mismo script SQL:

Matrícula: 00235
Nombre: Márcio Almeida Silva
Comision: 8%

y

Matrícula: 00236
Nombre: Cláudia Morais
Comision: 8%

Digite los comandos de inclusión.

Rta.

```sql
INSERT INTO TABLA_DE_VENDEDORES
(MATRICULA, NOMBRE, PORCENTAJE_COMISION)
VALUES
 ('00235','Márcio Almeida Silva',0.08);

INSERT INTO TABLA_DE_VENDEDORES
(MATRICULA, NOMBRE, PORCENTAJE_COMISION)
VALUES
('00236','Cláudia Morais',0.08);
```

### **Actualizando la información sobre los vendedores**

Recibimos la siguiente información:

Claudia Moral(00236) recibió un aumento y su comisión pasó a ser 11%. Joan Geraldo de la Fonseca(00233) reclamó que su nombre real es Joan Geraldo de la Fonseca Junior.

Efectúe estas correcciones en la base de datos.

Rta.

```sql
UPDATE TABLA_DE_VENDEDORES SET PORCENTAJE_COMISION = 0.11
WHERE MATRICULA = '00236';

UPDATE TABLA_DE_VENDEDORES SET NOME = 'Joan Geraldo de la Fonseca Junior'
WHERE MATRICULA = '00233';
```

### **Eliminando un vendedor**

El vendedor Joan Geraldo de la Fonseca Junior matrícula(00233) fue despedido.

Hay que retirarlo de la tabla de vendedores.

Rta.

```sql
DELETE FROM TABLA_DE_VENDEDORES WHERE MATRICULA = '00233';
```

### **Importancia de la clave primaria**

¿Cuál es el propósito de incluir una clave primaria al momento de crear una tabla?

Rta.

- Garantizar la integridad de los datos.
- Evitar la duplicidad en los registros.

### **Modificando la tabla de vendedores**

Vamos a incluir nuevos campos en la tabla de vendedores. Ellos serán la fecha de admisión. (Nombre del campo FECHA_ADMISION), y si el vendedor está o no de vacaciones. (Nombre del campo DE_VACACIONES). No olvides recrear la clave primaria y después incluye la siguiente información:

```txt
    Matrícula - 00235
    Nombre: Márcio Almeida Silva
    Comision: 8%
    Fecha de admisión: 15/08/2014
    ¿Está de vacaciones? No

    Matrícula - 00236
    Nombre: Cláudia Morais
    Comision: 8%
    Fecha de admisión: 17/09/2013
    ¿Está de vacaciones? Si

    Matrícula - 00237
    Nombre: Roberta Martins
    Comision: 11%
    Fecha de admisión: 18/03/2017
    ¿Está de vacaciones? Si

    Matrícula - 00238
    Nombre: Péricles Alves
    Comision: 11%
    Fecha de admisión: 21/08/2016
    ¿Está de vacaciones? No
```

Tips:

```txt
    Elimina la tabla.
    Crea la tabla nuevamente incluyendo los nuevos campos.
    Crea la clave primaria.
    Incluye los comandos de INSERT.
```

Rta.

```sql
DROP TABLE TABLA_DE_VENDEDORES;

CREATE TABLE TABLA_DE_VENDEDORES
( MATRICULA varchar(5),
NOMBE varchar(100),
PORCENTAJE_COMISION float,
FECHA_ADMISION date,
DE_VACACIONES bit);

ALTER TABLE TABLA_DE_VENDEDORES ADD PRIMARY KEY (MATRICULA);

INSERT INTO TABLA_DE_VENDEDORES
(MATRICULA, NOMBRE, FECHA_ADMISION, PORCENTAJE_COMISION, DE_VACACIONES) VALUES ('00235','Márcio Almeida Silva','2014-08-15',0.08,0);

INSERT INTO TABLA_DE_VENDEDORES
(MATRICULA, NOMBRE, FECHA_ADMISION, PORCENTAJE_COMISION, DE_VACACIONES) VALUES ('00236','Cláudia Morais','2013-09-17',0.08,1);

INSERT INTO TABLA_DE_VENDEDORES
(MATRICULA, NOMBRE, FECHA_ADMISION, PORCENTAJE_COMISION, DE_VACACIONES) VALUES ('00238','Pericles Alves','2016-08-21',0.11,0);
```

---

## **Consultando los datos**

### **Seleccionando a todos los vendedores**

Seleccionar el nombre y matrícula de los vendedores.

Rta.

```sql
SELECT NOMBRE, MATRICULA FROM TABLA_DE_VENDEDORES;
```

### **Seleccionando a un vendedor**

Verifica los datos registrados de la vendedora Claudia Morais.

```sql
SELECT * FROM TABLA_DE_VENDEDORES WHERE NOMBRE = 'Claudia Morais';
```

### **Seleccionando vendedores por el valor de la comisión**

Investiga cuáles son los vendedores que poseen comisión superior al 10%.

Rta.

```sql
SELECT * FROM TABLA_DE_VENDEDORES WHERE PORCENTAJE_COMISIÓN > 0.10;
```

### **Seleccionando a un vendedor por fecha**

Investiga cuáles son los vendedores que fueron admitidos en la empresa a partir del 2016.

Rta.

```sql
SELECT * FROM TABLA_DE_VENDEDORES WHERE YEAR(FECHA_ADMISION) >= 2016;
```

### **Selección compuesta**

Revisa cuáles son los vendedores que están de vacaciones y que fueron admitidos antes del 2016.

```sql
SELECT * FROM TABLA_DE_VENDEDORES WHERE YEAR(FECHA_ADMISION) < 2016;
```

### **Seleccionar a todos los vendedores**

Selecciona el nombre y matrícula de los vendedores.

```sql
SELECT NOMBRE, MATRICULA FROM TABLA_DE_VENDEDORES;
```
