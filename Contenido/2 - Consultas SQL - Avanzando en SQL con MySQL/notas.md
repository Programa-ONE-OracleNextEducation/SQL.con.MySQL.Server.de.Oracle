# **Consultas SQL - Avanzando en SQL con MySQL**

## **Configurando el ambiente y conociendo SQL**

### **Preparando el ambiente**

Aquí puedes descargar los archivos que necesitarás para iniciar con tu entrenamiento.

[Descargue los archivos en Github](https://github.com/ahcamachod/1827-consultas-sql-avanzando-en-sql-con-my-sql "GitHub") o haga clic [aquí](https://github.com/ahcamachod/1827-consultas-sql-avanzando-en-sql-con-my-sql/archive/refs/heads/main.zip "folder.zip") para descargarlos directamente.

### **Ventajas del lenguaje SQL**

¿Cuáles son las principales ventajas del lenguaje SQL?

Rta.

- El lenguaje SQL permite reducir los costos de entrenamiento. Como está estandarizado, un profesional que sepa SQL para una base de datos específica fácilmente podrá operar otra base de datos.
- Presenta una padronización entre las bases de datos. El padrón SQL es utilizado en la mayoría de las bases de datos relacionales del mercado.

### **Longevidad de SQL**

¿Por qué consideramos el estándar SQL como bueno para aplicaciones que van a funcionar a largo plazo?

Rta.

Fácil migración entre las bases de datos. Con el estándar SQL, las aplicaciones pueden ser fácilmente transportadas de una base de datos de un fabricante a otro.

### **Haga lo que hicimos en aula**

Llegó la hora de que sigas todos los pasos realizados por mí durante esta aula. En caso de que ya lo hayas hecho, excelente. Si aún no lo hiciste, es importante que ejecutes lo que fue visto en los videos para poder continuar con la siguiente aula.

1. Instala MySQL, conforme al video Instalando MySQL Server, del aula 1 del curso de Introducción a SQL con MySQL.
2. Abre MySQL Workbench. Utiliza la conexión LOCALHOST.
3. Haz clic con el Botón derecho del mouse sobre la lista de las bases de datos y escoge Create Schema...
4. Digita el nombre jugos_ventas. Haz clic en Apply dos veces.
5. Haz Download del archivo RecuperacionAmbiente.zip.
6. Descompacta el archivo.
7. Selecciona la pestaña Administration en el área Navigator.
8. Selecciona el link Data Import/Restore.
9. En la opción Import From Dump Project Folder escoge el directorio DumpJugosVentas.
10. Selecciona Start Import.
11. Verifica en la base de datos jugos_ventas si las tablas fueron creadas.
12. Existe otra manera de importar los archivos (Solo en caso de que el método anterior no funcione). Basta ejecutar todos los archivos .sql compactados en el archivo RecuperacionAmbiente.zip y listo.

---

## **Filtrando las consultas de los datos**

### **Visualizando el esquema de datos**

¿Por qué debemos visualizar el esquema de datos?

Rta.

Para determinar qué consultas vamos a hacer es fundamental conocer la estructura de la base de datos.

### **Listando los datos de una tabla**

¿Cuál es el comando SQL para listar todos los dados de una tabla?

Rta.

```sql
SELECT * FROM <name_table>
```

El \* determina que todos los campos de la tabla serán seleccionados.

### **Resuelve la estructura lógica**

Considera la siguiente expresión:

```sql
(NOT ((3 > 2) OR (4 >= 5)) AND (5 > 4) ) OR (9 > 0)}
```

¿Es verdadera o falsa?

Rta. Verdadero

```sh
(NOT ((3 > 2) OR (4 >= 5)) AND (5 > 4) ) OR (9 > 0)
(NOT ((Verdadero) OR (Falso)) AND (Verdadero) ) OR (Verdadero)
(NOT (Verdadero) AND (Verdadero) ) OR (Verdadero)
(Falso AND Verdadero) OR (Verdadero)
(Falso) OR (Verdadero)
Verdadero
```

### **Seleccionando ventas**

¿Cuál sería el comando SQL para seleccionar todos los ítems de facturas cuya cantidad sea mayor que 60 y precio menor o igual a 3?

Rta.

```sql
SELECT * FROM items_facturas
WHERE CANTIDAD > 60 AND PRECIO <= 3
```

### **Buscando clientes**

¿Cuántos clientes tienen apellidos que acaban con “ez”?

Rta.

Hay 6 apellidos que acaban con "ez"

---

## **Presentacion de los datos de una consulta**

### **Barrios de Ciudad de México**

¿Cuáles son los barrios en Ciudad de México que tienen clientes?

Rta.

Son 10 barrios (Del valle, Locaxco, Cuajimalpa, Condesa, Heroes de Padierna, Carola, Contadero, Floresta Coyoacán, Barrio del niño jesus y Ex hacienda Coapa)

```sql
SELECT DISTINCT BARRIO FROM tabla_de_clientes WHERE CIUDAD = 'Ciudad de México';
```

### **Observando una muestra de datos**

Queremos obtener las 10 primeras ventas del día 01/01/2017. ¿Cuál sería el comando SQL para obtener este resultado?

Rta.

```sql
SELECT * FROM facturas WHERE FECHA_VENTA = "2017-01-01" LIMIT 10;
```

### **Mayores ventas**

¿Cuál (o cuáles) fue (fueron) la(s) mayor(es) venta(s) del producto “Refrescante, 1 Litro, Frutilla/Limón”, en cantidad? (Obtenga este resultado utilizando 2 comandos SQL).

Rta.

Las consultas que debemos realizar son las siguientes:

```sql
SELECT CODIGO_DEL_PRODUCTO FROM tabla_de_productos
WHERE NOMBRE_DEL_PRODUCTO = "Refrescante" AND TAMANO = "1 Litro"
AND SABOR = "Frutilla/Limón";
```

Ahora, con el código del producto que es: 1101035, podemos consultar la cantidad:

```sql
SELECT * FROM items_facturas WHERE CODIGO_DEL_PRODUCTO = "1101035" ORDER BY CANTIDAD DESC;
```

Notaremos que la cantidad máxima vendida es de 99.

### **Número de ventas**

Aprovechando el ejercicio del video anterior ¿Cuántos ítems vendidos cuentan con la mayor cantidad del producto '1101035'?

Rta.

Las consultas que debemos realizar son las siguientes:

```sql
SELECT MAX(CANTIDAD) AS CANTIDAD_MAXIMA FROM items_facturas WHERE CODIGO_DEL_PRODUCTO = "1101035";
```

Notaremos que la cantidad máxima vendida es de 99. Así, podemos entonces ejecutar el siguiente comando:

```sql
SELECT COUNT(*) FROM items_facturas WHERE CODIGO_DEL_PRODUCTO = "1101035" AND CANTIDAD = 99;
```

Notaremos que la cantidad de ítems vendida es de 79.

### **Clientes que realizaron compras en 2016**

¿Quiénes fueron los clientes que realizaron más de 2000 compras en 2016?

Rta.

La consulta que debemos realizar es la siguiente:

```sql
SELECT DNI, COUNT(*) FROM facturas
WHERE YEAR(FECHA_VENTA) = 2016
GROUP BY DNI
HAVING COUNT(*) > 2000;
```

Nos devolverá 3 clientes.

### **Clasificando el número de ventas**

Registre el año de nacimiento de los clientes y clasifíquelos de la siguiente manera:

Nacidos antes de 1990= Viejos, nacidos entre 1990 y 1995= Jóvenes y nacidos después de 1995= Niños. Liste el nombre del cliente y esta clasificación.

Rta.

El comando que debemos ejecutar es el siguiente:

```sql
SELECT NOMBRE,
CASE
    WHEN YEAR(fecha_de_nacimiento) < 1990 THEN 'Viejos'
    WHEN YEAR(fecha_de_nacimiento) >= 1990
    AND YEAR(fecha_de_nacimiento) <= 1995 THEN 'Jóvenes'
    ELSE 'Niños'
END AS CLASIFICACION_EDAD
FROM tabla_de_clientes;
```

---

## **Uniendo tablas y consultas**

### **Obteniendo la facturación anual**

Obtén la facturación anual de la empresa. Ten en cuenta que el valor financiero de las ventas consiste en multiplicar la cantidad por el precio.

Rta.

El comando que debemos ejecutar es el siguiente:

```sql
SELECT YEAR(FECHA_VENTA), SUM(CANTIDAD * PRECIO) AS FACTURACION
FROM facturas F
INNER JOIN
items_facturas IFa
ON F.NUMERO = IFa.NUMERO
GROUP BY YEAR(FECHA_VENTA);
```

### **Seleccionando el tipo de JOIN**

Complete el siguiente comando de SQL:

```sql
SELECT * FROM TAB1 /*????*/ TAB2 ON TAB1.COD = TAB2.COD
```

Tip: Queremos ver todos los registros de la TAB2 y solamente los que corresponden a la TAB1.

Rta.

```sql
RIGHT JOIN
```

Este es el tipo de JOIN que nos va a devolver lo que queremos consultar.

### **Nueva selección del tipo de JOIN**

Complete el siguiente comando de SQL:

```sql
SELECT * FROM TAB1 /*????*/ TAB2 ON TAB1.COD = TAB2.COD
```

Tip: Queremos ver todos los registros de la TAB1 y todos los de la TAB2.

Rta.

```sql
FULL JOIN
```

Este es el tipo de JOIN que nos va a devolver lo que queremos consultar.

### **Diferencias entre UNION y UNION ALL**

Cuando queremos listar todos los registros de dos o más tablas, listando incluso los repetidos, ¿Cuál tipo de UNION empleamos?

Rta.

```sql
UNION ALL
```

Así es, este comando nos devuelve todos los registros, incluso los repetidos.

### **Relación entre HAVING y Subconsulta**

Cuál sería la consulta utilizando la subconsulta que sería equivalente a:

```sql
SELECT DNI, COUNT(*) FROM facturas
WHERE YEAR(FECHA_VENTA) = 2016
GROUP BY DNI
HAVING COUNT(*) > 2000;
```

Rta.

El comando que debemos ejecutar es el siguiente:

```sql
SELECT X.DNI, X.CONTADOR FROM
(SELECT DNI, COUNT(*) AS CONTADOR FROM facturas
WHERE YEAR(FECHA_VENTA) = 2016
GROUP BY DNI) X WHERE X.CONTADOR > 2000;
```

### **Características de la vista**

Considera las siguientes opciones, y escoge la que sea falsa:

Rta.

1. No podemos crear una vista de otra vista.
2. Podemos crear una vista utilizando comandos o el asistente de Workbench. (FALSO)
3. Podemos crear una vista de cualquier comando SQL.

---

## **Funciones de MySQL**

### **Listando la dirección completa**

Haz una consulta listando el nombre del cliente y la dirección completa (Con calle, barrio, ciudad y estado).

Rta.

El comando que debes ejecutar es el siguiente:

```sql
SELECT NOMBRE, CONCAT(DIRECCION_1, ' ', BARRIO, ' ', CIUDAD, ' ', ESTADO) AS COMPLETO FROM tabla_de_clientes;
```

### **Edad de los clientes**

Haz una consulta que muestre el nombre y la edad actual del cliente.

Rta.

El comando que debes ejecutar es el siguiente:

```sql
SELECT NOMBRE, TIMESTAMPDIFF(YEAR, FECHA_DE_NACIMIENTO, CURDATE()) AS    EDAD
FROM  tabla_de_clientes;
```

### **Formato de facturación**

En la tabla de facturas tenemos el valor del impuesto. En la tabla de ítems tenemos la cantidad y la facturación. Calcula el valor del impuesto pago en el año de 2016 redondeando al menor entero.

El comando que debes ejecutar es el siguiente:

```sql
SELECT YEAR(FECHA_VENTA), FLOOR(SUM(IMPUESTO * (CANTIDAD * PRECIO)))
AS RESULTADO
FROM facturas F
INNER JOIN items_facturas IFa ON F.NUMERO = IFa.NUMERO
WHERE YEAR(FECHA_VENTA) = 2016
GROUP BY YEAR(FECHA_VENTA);
```

### **Listando con expresión natural**

Queremos construir un SQL cuyo resultado sea, para cada cliente:

“El cliente Pepito Pérez facturó 120000 en el año 2016”.

Solamente para el año 2016.

Rta.

El comando que debes ejecutar es el siguiente:

```sql
SELECT CONCAT('El cliente ', TC.NOMBRE, ' facturó ',
CONVERT(SUM(IFa.CANTIDAD * IFa.precio), CHAR(20))
 , ' en el año ', CONVERT(YEAR(F.FECHA_VENTA), CHAR(20))) AS FRASE FROM facturas F
INNER JOIN items_facturas IFa ON F.NUMERO = IFa.NUMERO
INNER JOIN tabla_de_clientes TC ON F.DNI = TC.DNI
WHERE YEAR(FECHA_VENTA) = 2016
GROUP BY TC.NOMBRE, YEAR(FECHA_VENTA);
```

---

## **Ejemplos de informes**

### **Realizando una consulta al informe**

En esta aula construimos un informe que presentó a los clientes que tenían ventas inválidas. Complementa este informe listando solamente a los que tuvieron ventas inválidas y calcula la diferencia entre el límite de venta máximo y la cantidad vendida en porcentuales.

Tips:

- Utiliza el comando SQL empleado al final del video.
- Filtra solamente las líneas donde: (A.CANTIDAD_VENDIDA - A.CANTIDAD_MAXIMA) < 0
- Lista el campo X.CANTIDAD_LIMITE
- Crea nuevo campo ejecutando la fórmula: (1 - (X.QUANTIDADE_LIMITE/X.QUANTIDADE_VENDAS)) \* 100.

Rta.

Este desafío puede tener diversas respuestas. A continuación te muestro una de ellas:

```sql
SELECT A.DNI, A.NOMBRE, A.MES_AÑO,
A.CANTIDAD_VENDIDA - A.CANTIDAD_MAXIMA AS DIFERENCIA,
CASE
   WHEN  (A.CANTIDAD_VENDIDA - A.CANTIDAD_MAXIMA) <= 0 THEN 'Venta Válida'
   ELSE 'Venta Inválida'
END AS STATUS_VENTA, ROUND((1 - (A.CANTIDAD_MAXIMA/A.CANTIDAD_VENDIDA)) * 100,2) AS PORCENTAJE
 FROM(
SELECT F.DNI, TC.NOMBRE, DATE_FORMAT(F.FECHA_VENTA, "%m - %Y") AS MES_AÑO,
SUM(IFa.CANTIDAD) AS CANTIDAD_VENDIDA,
MAX(VOLUMEN_DE_COMPRA)/10 AS CANTIDAD_MAXIMA
FROM facturas F
INNER JOIN
items_facturas IFa
ON F.NUMERO = IFa.NUMERO
INNER JOIN
tabla_de_clientes TC
ON TC.DNI = F.DNI
GROUP BY
F.DNI, TC.NOMBRE, DATE_FORMAT(F.FECHA_VENTA, "%m - %Y"))A
WHERE (A.CANTIDAD_MAXIMA - A.CANTIDAD_VENDIDA) < 0;
```

### **Realizando una nueva consulta al informe**

En esta aula construimos un informe que presentó a los clientes que tenían ventas inválidas. Ahora lista solamente a los que tuvieron ventas inválidas en el año 2018 excediendo más del 50% de su límite permitido por mes. Calcula la diferencia entre el límite de venta máximo y la cantidad vendida, en porcentuales.

Tips:

- Te puedes apoyar en el código que realizaste para el desafío anterior.

Rta.

Este desafío puede tener diversas respuestas. A continuación te muestro una de ellas:

```sql
SELECT A.DNI, A.NOMBRE, A.MES_AÑO,
A.CANTIDAD_VENDIDA - A.CANTIDAD_MAXIMA AS DIFERENCIA,
CASE
   WHEN  (A.CANTIDAD_VENDIDA - A.CANTIDAD_MAXIMA) <= 0 THEN 'Venta Válida'
   ELSE 'Venta Inválida'
END AS STATUS_VENTA, ROUND((1 - (A.CANTIDAD_MAXIMA/A.CANTIDAD_VENDIDA)) * 100,2) AS PORCENTAJE
 FROM(
SELECT F.DNI, TC.NOMBRE, DATE_FORMAT(F.FECHA_VENTA, "%m - %Y") AS MES_AÑO,
SUM(IFa.CANTIDAD) AS CANTIDAD_VENDIDA,
MAX(VOLUMEN_DE_COMPRA)/10 AS CANTIDAD_MAXIMA
FROM facturas F
INNER JOIN
items_facturas IFa
ON F.NUMERO = IFa.NUMERO
INNER JOIN
tabla_de_clientes TC
ON TC.DNI = F.DNI
GROUP BY
F.DNI, TC.NOMBRE, DATE_FORMAT(F.FECHA_VENTA, "%m - %Y"))A
WHERE (A.CANTIDAD_MAXIMA - A.CANTIDAD_VENDIDA) < 0 AND ROUND((1 - (A.CANTIDAD_MAXIMA/A.CANTIDAD_VENDIDA)) * 100,2) > 50
AND A.MES_AÑO LIKE "%2018";
```

### **Ventas porcentuales por tamaño**

Modifica el informe pero ahora para ver el ranking de las ventas por tamaño.

Tips:

- Puede parecer difícil pero este es el ejercicio más fácil de resolver.

Rta.

Lo único que debes hacer es tomar el informe, y en vez de utilizar el sabor como parámetro, utilizas tamaño. El restante de la consulta permanece igual:

```sql
SELECT VENTAS_TAMANO.TAMANO, VENTAS_TAMANO.AÑO, VENTAS_TAMANO.CANTIDAD_TOTAL,
ROUND((VENTAS_TAMANO.CANTIDAD_TOTAL/VENTA_TOTAL.CANTIDAD_TOTAL)*100,2)
AS PORCENTAJE FROM (
SELECT P.TAMANO, SUM(IFa.CANTIDAD) AS CANTIDAD_TOTAL,
YEAR(F.FECHA_VENTA) AS AÑO FROM
tabla_de_productos P
INNER JOIN
items_facturas IFa
ON P.CODIGO_DEL_PRODUCTO = IFa.CODIGO_DEL_PRODUCTO
INNER JOIN
facturas F
ON F.NUMERO = IFa.NUMERO
WHERE YEAR(F.FECHA_VENTA) = 2016
GROUP BY P.TAMANO, YEAR(F.FECHA_VENTA)
ORDER BY SUM(IFa.CANTIDAD) DESC) VENTAS_TAMANO
INNER JOIN (
SELECT SUM(IFa.CANTIDAD) AS CANTIDAD_TOTAL,
YEAR(F.FECHA_VENTA) AS AÑO FROM
tabla_de_productos P
INNER JOIN
items_facturas IFa
ON P.CODIGO_DEL_PRODUCTO = IFa.CODIGO_DEL_PRODUCTO
INNER JOIN
facturas F
ON F.NUMERO = IFa.NUMERO
WHERE YEAR(F.FECHA_VENTA) = 2016
GROUP BY YEAR(F.FECHA_VENTA)) VENTA_TOTAL
ON VENTA_TOTAL.AÑO = VENTAS_TAMANO.AÑO
ORDER BY VENTAS_TAMANO.CANTIDAD_TOTAL DESC;
```

### **Haga lo que hicimos en aula**

Llegó la hora de que sigas todos los pasos realizados por mí durante esta aula. En caso de que ya lo hayas hecho, excelente. Si aún no lo hiciste, es importante que ejecutes lo que fue visto en los videos para poder continuar con la siguiente aula.

1. Vamos a poner en práctica nuestro conocimiento.

2. Primero, generamos una selección que determina si las ventas mensuales por cliente son válidas o no. Consideramos válidas las ventas por debajo de la cantidad límite e inválidas por encima de la cantidad límite existente en el registro del cliente. La consulta se muestra a continuación:

```sql
SELECT A.DNI, A.NOMBRE, A.MES_AÑO,
A.CANTIDAD_VENDIDA - A.CANTIDAD_MAXIMA AS DIFERENCIA,
CASE
   WHEN  (A.CANTIDAD_VENDIDA - A.CANTIDAD_MAXIMA) <= 0 THEN 'Venta Válida'
   ELSE 'Venta Inválida'
END AS STATUS_VENTA
 FROM(
SELECT F.DNI, TC.NOMBRE, DATE_FORMAT(F.FECHA_VENTA, "%m - %Y") AS MES_AÑO,
SUM(IFa.CANTIDAD) AS CANTIDAD_VENDIDA,
MAX(VOLUMEN_DE_COMPRA)/10 AS CANTIDAD_MAXIMA
FROM facturas F
INNER JOIN
items_facturas IFa
ON F.NUMERO = IFa.NUMERO
INNER JOIN
tabla_de_clientes TC
ON TC.DNI = F.DNI
GROUP BY
F.DNI, TC.NOMBRE, DATE_FORMAT(F.FECHA_VENTA, "%m - %Y"))A;
```

3. Otro ejemplo de informe es el que determina la venta por sabores, para el año de 2016, presentando el porcentaje de participación de cada uno de estos sabores, ordenados.

```sql
SELECT VENTAS_SABOR.SABOR, VENTAS_SABOR.AÑO, VENTAS_SABOR.CANTIDAD_TOTAL,
ROUND((VENTAS_SABOR.CANTIDAD_TOTAL/VENTA_TOTAL.CANTIDAD_TOTAL)*100,2)
AS PORCENTAJE FROM (
SELECT P.SABOR, SUM(IFa.CANTIDAD) AS CANTIDAD_TOTAL,
YEAR(F.FECHA_VENTA) AS AÑO FROM
tabla_de_productos P
INNER JOIN
items_facturas IFa
ON P.CODIGO_DEL_PRODUCTO = IFa.CODIGO_DEL_PRODUCTO
INNER JOIN
facturas F
ON F.NUMERO = IFa.NUMERO
WHERE YEAR(F.FECHA_VENTA) = 2016
GROUP BY P.SABOR, YEAR(F.FECHA_VENTA)
ORDER BY SUM(IFa.CANTIDAD) DESC) VENTAS_SABOR
INNER JOIN (
SELECT SUM(IFa.CANTIDAD) AS CANTIDAD_TOTAL,
YEAR(F.FECHA_VENTA) AS AÑO FROM
tabla_de_productos P
INNER JOIN
items_facturas IFa
ON P.CODIGO_DEL_PRODUCTO = IFa.CODIGO_DEL_PRODUCTO
INNER JOIN
facturas F
ON F.NUMERO = IFa.NUMERO
WHERE YEAR(F.FECHA_VENTA) = 2016
GROUP BY YEAR(F.FECHA_VENTA)) VENTA_TOTAL
ON VENTA_TOTAL.AÑO = VENTAS_SABOR.AÑO
ORDER BY VENTAS_SABOR.CANTIDAD_TOTAL DESC;
```
