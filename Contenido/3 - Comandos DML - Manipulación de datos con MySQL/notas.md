# **Comandos DML - Manipulacion de datos con MySQL**

## **Modelaje de la base de datos**

### **La entidad básica**

De las siguientes opciones ¿Cuál sería la entidad básica de MySQL?

Rta.

Tabla. Efectivamente, la tabla es la entidad básica de MySQL.

### **La clave primaria**

¿Qué es una clave primaria?

Rta.

Es una restricción a la base que determina los campos cuyos valores no se pueden repetir dentro de las líneas de las tablas. Esta es la definición de clave primaria.

### **Requisitos de una base de datos**

Marca las alternativas verdaderas que representan requisitos para el inicio de la construcción de una base de datos.

Rta.

- Entender las reglas del negocio. Este es uno de los requisitos mencionados en el video.
- Diseño de un modelo que corresponda con la realidad. Este es uno de los requisitos mencionados en el video.

### **Pasos importantes para la creación de un modelo conceptual**

Marca las alternativas verdaderas que representan los pasos para la creación de un modelo conceptual.

- Establecer la cardinalidad entre las entidades. Este es uno de los requisitos mencionados en el video.
- Construcción del diagrama Entidad-Relación. Este es uno de los requisitos mencionados en el video.

---

## **Creando la estructura de la base de datos**

### **Parámetro para la creación de la base de datos**

¿Qué significa el parámetro DEFAULT CHARACTER SET usado en la creación de la base de datos en MySQL?

Rta.

Hace referencia a la tabla de caracteres que será utilizada al almacenar campos de texto. Aquí especificamos la tabla ASCII de caracteres que serán empleados en la base de datos.

### **Creando claves externas**

Crea, por línea de comando, la clave externa que relaciona a la tabla facturas con items_facturas a través del campo NUMERO.

Rta.

```sql
USE ventas_jugos;

CREATE TABLE items_facturas
(NUMERO VARCHAR(5) NOT NULL,
CODIGO VARCHAR(10) NOT NULL,
CANTIDAD INT,
PRECIO FLOAT,
PRIMARY KEY (NUMERO, CODIGO));

ALTER TABLE items_facturas ADD CONSTRAINT FK_FACTURAS
FOREIGN KEY (NUMERO)
REFERENCES facturas(NUMERO);
```

### **Cambiando el nombre de la tabla**

La gerencia de la empresa de jugos realizó la solicitud de cambiar los nombres de las tablas colocando el prefijo tb\_ antes de cada nombre.

Rta.

```sql
ALTER TABLE factura RENAME tb_factura;
ALTER TABLE cliente RENAME tb_cliente;
ALTER TABLE items_facturas RENAME tb_items_facturas;
```

---

## **Incluyendo datos en las tablas**

### **Inclusión de registros en las tablas**

Incluye los siguientes registros en la tabla de clientes:

- DNI: 9283760794, NOMBRE: Edson Calisaya, DIRECCION: Sta Úrsula Xitla, BARRIO: Barrio del Niño Jesús, CIUDAD: Ciudad de México, ESTADO: EM, CP: 22002002, FECHA DE NACIMIENTO: 1995-01-07, EDAD: 25, SEXO: M, LIMITE DE CREDITO: 150000, VOLUMEN DE COMPRA: 250000, PRIMERA COMPRA: Sí.
- DNI: 7771579779, NOMBRE: Marcelo Perez, DIRECCION: F.C. de Cuernavaca 13, BARRIO: Carola, CIUDAD: Ciudad de México, ESTADO: EM, CP: 88202912, FECHA DE NACIMIENTO: 1992-01-25, EDAD: 29, SEXO: M, LIMITE DE CREDITO: 70000, VOLUMEN DE COMPRA: 160000, PRIMERA COMPRA: Sí.
- DNI: 5576228758, NOMBRE: Patricia Olivera, DIRECCION: Pachuca 75, BARRIO: Condesa, CIUDAD: Ciudad de México, ESTADO: EM, CP: 88192029, FECHA DE NACIMIENTO: 1995-01-14 , EDAD: 25, SEXO: F, LIMITE DE CREDITO: 70000, VOLUMEN DE COMPRA: 160000, PRIMERA COMPRA: Sí.

Rta.

```sql
INSERT INTO TABLA_DE_CLIENTES (DNI, NOMBRE, DIRECCION, BARRIO, CIUDAD, ESTADO, CP, FECHA_NACIMIENTO, EDAD, SEXO, LIMITE_CREDITO, VOLUMEN_COMPRA, PRIMERA_COMPRA) VALUES ('9283760794', 'Edson Calisaya', 'Sta Úrsula Xitla', 'Barrio del Niño Jesús', 'Ciudad de México', 'EM', '22002002', '1995-01-07', 25, 'M', 150000, 250000, 1);
INSERT INTO TABLA_DE_CLIENTES (DNI, NOMBRE, DIRECCION, BARRIO, CIUDAD, ESTADO, CP, FECHA_NACIMIENTO, EDAD, SEXO, LIMITE_CREDITO, VOLUMEN_COMPRA, PRIMERA_COMPRA) VALUES ('7771579779', 'Marcelo Perez', 'F.C. de Cuernavaca 13', 'Carola', 'Ciudad de México', 'EM', '88202912', '1992-01-25', 29, 'M', 120000, 200000, 1);
INSERT INTO TABLA_DE_CLIENTES (DNI, NOMBRE, DIRECCION, BARRIO, CIUDAD, ESTADO, CP, FECHA_NACIMIENTO, EDAD, SEXO, LIMITE_CREDITO, VOLUMEN_COMPRA, PRIMERA_COMPRA) VALUES ('5576228758', 'Patricia Olivera', 'Pachuca 75',  'Condesa', 'Ciudad de México', 'EM', '88192029', '1995-01-14', 25, 'F', 70000, 160000, 1);
```

### **Inclusión de registros a partir de otra tabla**

Incluye todos los clientes en la tabla tb_clientes basado en los registros de la tabla tabla_de_clientes de la base jugos_ventas.

Observación: Atención al nombre de los campos y recuerda que ya incluimos 3 clientes en nuestra tabla durante el ejercicio anterior.

Rta.

USE ventas_jugos;

```sql
INSERT INTO tb_clientes
SELECT DNI, NOMBRE, DIRECCION_1 AS DIRECCION,
BARRIO, CIUDAD, ESTADO, CP, FECHA_DE_NACIMIENTO
AS FECHA_NACIMIENTO, EDAD, SEXO, LIMITE_DE_CREDITO
AS LIMITE_CREDITO, VOLUMEN_DE_COMPRA AS VOLUMEN_COMPRA,
PRIMERA_COMPRA FROM jugos_ventas.tabla_de_clientes
WHERE DNI NOT IN (SELECT DNI FROM tb_clientes);
```

---

## **Alterando y excluyendo datos existentes en las tablas**

### **Alterando datos en la tabla de clientes**

Actualiza la dirección del cliente con el DNI 5840119709 Colocando como nueva dirección Jorge Emilio 23, barrio San Antonio, ciudad Guadalajara, Estado de Jalisco y el CP 44700000.

Rta.

```sql
UPDATE tb_clientes SET
DIRECCION = 'Jorge Emilio 23',
BARRIO = 'San Antonio',
CIUDAD = 'Guadalajara',
ESTADO = 'JC',
CEP = '44700000'
WHERE DNI = '5840119709'
```

### **Modificando el volumen de compra**

Podemos observar que los vendedores se encuentran en barrios asociados a ellos. Vamos a aumentar en 30% el volumen de compra de los clientes que tienen, en sus direcciones, barrios donde los vendedores cuentan con oficinas.

Tip: Vamos a usar un UPDATE con FROM apoyándonos con el siguiente INNER JOIN:

```sql
SELECT A.DNI FROM tb_clientes A
INNER JOIN tb_vendedor B
ON A.BARRIO = B.BARRIO
```

Rta.

```sql
UPDATE tb_clientes A
INNER JOIN
tb_vendedor B
ON A.BARRIO = B.BARRIO
SET A.VOLUMEN_COMPRA = A.VOLUMEN_COMPRA * 1.3
```

### **Excluyendo facturas**

Vamos a excluir las facturas (Apenas el encabezado) cuyos clientes tengan menos de 18 años.

Tip: Usaremos la siguiente consulta:

```sql
SELECT A.DNI FROM tb_clientes A
INNER JOIN tb_vendedor B
ON A.BARRIO = B.BARRIO
```

Puedes usar una sintaxis semejante a la usada en UPDATE con FROM.

Rta.

```sql
DELETE A FROM tb_facturas A
INNER JOIN
tb_clientes B
ON A.DNI = B.DNI
WHERE B.EDAD < 18;
```

### **Cuidado al alterar o excluir datos**

¿Por qué debemos tener cuidado al usar comandos UPDATE o DELETE sin condiciones con la cláusula WHERE?

Rta.

Porque no lograremos recuperar los datos en caso que ejecutemos un comando sin la cláusula WHERE sobre toda la tabla.

### **Utilizando ROLLBACK 2 veces**

¿Se presentarán errores al utilizar el comando ROLLBACK dos veces?

Rta.

No, porque podemos usar comandos ROLLBACK simultáneos en el mismo script. Podemos usar ROLLBACKs seguidos en un mismo script, aunque no haya iniciado una nueva transacción. Pero, el comando se torna inútil porque nada va a suceder.

---

## **Autoincremento, patrones y triggers**

### **Valor de la secuencia**

¿Cuál será el valor final de la secuencia tras la ejecución de los siguientes comandos?

```sql
CREATE TABLE TAB_IDENTITY2 (ID INT AUTO_INCREMENT, DESCRIPTOR VARCHAR(20), PRIMARY KEY(ID));
INSERT INTO TAB_IDENTITY2 (DESCRIPTOR) VALUES ('CLIENTE1');
INSERT INTO TAB_IDENTITY2 (DESCRIPTOR) VALUES ('CLIENTE2');
INSERT INTO TAB_IDENTITY2 (DESCRIPTOR) VALUES ('CLIENTE3');
INSERT INTO TAB_IDENTITY2 (ID, DESCRIPTOR) VALUES (NULL, 'CLIENTE4');
DELETE FROM TAB_IDENTITY2 WHERE ID = 3;
INSERT INTO TAB_IDENTITY2 (ID, DESCRIPTOR) VALUES (NULL, 'CLIENTE6');
INSERT INTO TAB_IDENTITY2 (ID, DESCRIPTOR) VALUES (NULL, 'CLIENTE7');
DELETE FROM TAB_IDENTITY2 WHERE ID = 2;
```

Rta.

El valor final de la secuencia será 6.

### **Comportamiento de un campo DEFAULT al utilizar INSERT**

¿Un campo que fue definido como estándar al momento de la creación de la tabla, presentará qué tipo de comportamiento durante el INSERT?

Rta.

Si el mismo no aparece en el comando INSERT, será incluido el valor DEFAULT que fue especificado al momento de la creación de la tabla.

### **TRIGER**

Un trigger es un tipo especial de procedimiento almacenado que se ejecuta cuando un evento ocurre en el servidor de la base de datos.

### **Calculando la edad del cliente**

El siguiente comando SQL calcula la edad actual del cliente:

```sql
SELECT DNI, EDAD, FECHA_NACIMIENTO, timestampdiff(YEAR, FECHA_NACIMIENTO, NOW()) AS ANOS FROM tb_clientes;
```

Construye un TRIGGER (Lo llamaremos TG_EDAD_CLIENTES_INSERT) que actualiza las edades de los clientes, en la tabla de clientes, siempre que la tabla sufra una inclusión.

```sql
DELIMITER //
CREATE TRIGGER TG_EDAD_CLIENTES_INSERT
BEFORE INSERT ON tb_clientes
FOR EACH ROW BEGIN
SET NEW.EDAD = timestampdiff(YEAR, NEW.FECHA_NACIMIENTO, NOW());
END //
```

### **Comandos DML y TRIGGERs**

¿Cuáles de los siguientes comandos pueden trabajar con TRIGGERs en MySQL?

Rta.

Los comandos INSERT, UPDATE y DELETE pueden trabajar con TRIGGERs en MySQL.

### **Otras formas de manipulación de datos**

¿Cuál de los siguientes lenguajes de programación no sirve para manipular datos?

Rta.

Todos los programas anteriormente (.NET, Java y Python) mencionados son muy útiles para trabajar con manipulación de datos.
