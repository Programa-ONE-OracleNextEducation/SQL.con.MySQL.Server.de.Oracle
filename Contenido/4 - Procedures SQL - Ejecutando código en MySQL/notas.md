# **Procedure SQL - Ejecutando código en MySQL**

## **Preparando el Ambiente**

### **Preparando el ambiente**

Aquí puedes descargar los archivos que necesitarás para iniciar con tu entrenamiento.

[Descargue los archivos en Github](https://github.com/alura-es-cursos/1833-procedures-sql-ejecutando-codigo-en-mysql "GitHub") o haga clic [aquí](https://github.com/alura-es-cursos/1833-procedures-sql-ejecutando-codigo-en-mysql/archive/refs/heads/main.zip "folder.zip") para descargarlos directamente.

---

## **Stored Procedures básico**

### **Cambio del Delimitador**

¿Por qué hay cambios en el delimitador para uno o más caracteres (como por ejemplo $$) cuando creamos un Stored Procedure?

Rta.

Porque si continuamos empleando el punto y coma se presentará un conflicto entre entre el punto y coma del Stored Procedure y el de los comandos para crearlo.

### **Estructura para crear los Stored Procedures**

¿Cuál es la estructura de comando para la creación de un Stored Procedure?

Rta.

```sql
CREATE PROCEDURE

procedure_nome(parameters)

BEGIN

DECLARE declaration_statement;

...
executable_statement;
...

END;
```

### **Comando para alterar un Stored Procedure**

Cuál es el comando para alterar un Stored Procedure?

Rta.

```sql
DROP PROCEDURE
CREATE PROCEDURE
```

No existe un comando para alterar un Stored Procedure, su cuerpo y sus parámetros. Para ello, tenemos que borrarlo con el comando DROP y crearlo de nuevo.

### **Estructura de comando para la creación de un Stored Procedure**

Crea un SP con las siguientes 4 variables y que al utilizar el comando CALL, las mismas puedan ser visualizadas.

```txt
Nombre: Cliente.
Tipo: Caracteres con 10 posiciones.
Valor: Juan.

Nombre: Edad.
Tipo: Entero.
Valor: 10.

Nombre: Fecha_Nacimiento.
Tipo: Fecha.
Valor: 10/01/2007.

Nombre: Costo.
Tipo: Número con casillas decimales.
Valor: 10.23.
```

Rta.

```sql
CREATE PROCEDURE `desafio_1`()
BEGIN
DECLARE Cliente VARCHAR(10);
DECLARE Edad INT;
DECLARE Fecha_Nacimiento DATE;
DECLARE Costo FLOAT;

SET Cliente = 'Juan';
SET Edad = 10;
SET Fecha_Nacimiento = '2017-01-10';
SET Costo = 10.23;

SELECT Cliente;
SELECT Edad;
SELECT Fecha_Nacimiento;
SELECT Costo;

END
```

---

## **Interacciones con la base de datos**

### **Actualizando la edad**

Crea un SP que actualice la edad de los clientes. Recuerda que el comando para calcular la edad, en la tabla tabla_de_clientes es:

Rta.

```sql
TIMESTAMPDIFF(YEAR, FECHA_DE_NACIMIENTO, CURDATE()) as EDAD
```

Puedes nombrar el SP así: calcula_edad.

Rta.

```sql
DELIMITER $$
CREATE PROCEDURE `calcula_edad`()
BEGIN
UPDATE tabla_de_clientes SET EDAD =  TIMESTAMPDIFF(YEAR, FECHA_DE_NACIMIENTO, CURDATE());
END $$
```

### **Actualizando la comisión**

Crea un SP para reajustar el % de comisión de los vendedores. Incluye como parámetro del SP el %, expresado en valor (Ex: 0,90).

Nombre del Stored Procedure: reajuste_comision.

```sql
DELIMITER $$
CREATE PROCEDURE `reajuste_comision`(vporcentaje FLOAT)
BEGIN
UPDATE tabla_de_vendedores SET PORCENTAJE_COMISION =  PORCENTAJE_COMISION * (1 + vporcentaje);
END $$
```

### **Comando para tratar los errores**

¿Cuál es el comando para tratar los errores?

Rta.

```sql
DECLARE EXIT HANDLER FOR
```

### **Usando SELECT para atribuir valores**

Crea una variable llamada N_FACTURAS y atribuye a ella el número de facturas del día 01/01/2017. Muestra en el output del script el valor de la variable. (Nombra este Stored Procedure como cantidad_facturas).

Rta.

```sql
DELIMITER $$
CREATE PROCEDURE `cantidad_facturas`()
BEGIN
DECLARE N_FACTURAS INTEGER;
SELECT COUNT(*) INTO N_FACTURAS FROM facturas WHERE
FECHA_VENTA = '2017-01-01';
SELECT N_FACTURAS;
END $$
```

---

## **Control de flujo**

### **Evaluando la cantidad de facturas**

Crea un Stored Procedure que, basado en una fecha, calcule el número de facturas. Si aparecen más de 70 facturas exhibimos el mensaje: ‘Muchas facturas’. En otro caso, exhibimos el mensaje ‘Pocas facturas’. También, debe exhibir el número de facturas. Llamaremos este Stored Procedure como: evaluacion_facturas.

La fecha empleada para evaluar la cantidad de facturas será un parámetro del Stored Procedure.

```sql
DELIMITER $$
CREATE PROCEDURE `evaluacion_facturas`(vfecha DATE)
BEGIN
DECLARE mensaje VARCHAR(50);
DECLARE N_FACTURAS INTEGER;
SELECT COUNT(*) INTO N_FACTURAS FROM facturas WHERE
FECHA_VENTA = vfecha;
IF N_FACTURAS > 70
THEN SET mensaje = 'Muchas facturas' ;
ELSE SET mensaje = 'Pocas facturas' ;
END IF;
SELECT mensaje;
END $$
```

### **Facturación anual**

¡Desafío! Observa la siguiente consulta:

```sql
SELECT SUM(B.CANTIDAD * B.PRECIO) AS TOTAL_VENTA FROM
facturas A INNER JOIN items_facturas B
ON A.NUMERO = B.NUMERO
WHERE A.FECHA_VENTA = '20170301';
```

Esta consulta devuelve el valor de la facturación en una fecha determinada.

Construye un Stored Procedure llamado comparacion_ventas que compare las ventas en dos fechas distintas (Estas fechas serán los parámetros de la SP). Si la variación porcentual de estas ventas es mayor al 10% la respuesta debe ser ‘Verde’. Si está entre -10% y 10% debe retornar ‘Amarillo’. Si la variación es menor al -10% debe retornar ‘Rojo’.

```sql
DELIMITER $$
CREATE PROCEDURE `comparacion_ventas`(vfecha1 DATE, vfecha2 DATE)
BEGIN
DECLARE facturacion_inicial FLOAT;
DECLARE facturacion_final FLOAT;
DECLARE variacion float;
SELECT SUM(B.CANTIDAD * B.PRECIO) INTO facturacion_inicial  FROM
facturas A INNER JOIN items_facturas B
ON A.NUMERO = B.NUMERO
WHERE A.FECHA_VENTA = vfecha1;
SELECT SUM(B.CANTIDAD * B.PRECIO) INTO facturacion_final  FROM
facturas A INNER JOIN items_facturas B
ON A.NUMERO = B.NUMERO
WHERE A.FECHA_VENTA = vfecha2;
SET variacion = ((facturacion_final / facturacion_inicial) -1) * 100;
IF variacion > 10 THEN
SELECT 'Verde';
ELSEIF variacion >= -10 AND variacion <= 10 THEN
SELECT 'Amarillo';
ELSE
SELECT 'Rojo';
END IF;
END $$
```

### **Facturación anual usando CASE condicional**

Realiza el desafío anterior utilizando CASE condicional.

```sql
DELIMITER $$
CREATE PROCEDURE `comparacion_ventas_case`(vfecha1 DATE, vfecha2 DATE)
BEGIN
DECLARE facturacion_inicial FLOAT;
DECLARE facturacion_final FLOAT;
DECLARE variacion float;
DECLARE mensaje VARCHAR(20);
SELECT SUM(B.CANTIDAD * B.PRECIO) INTO facturacion_inicial  FROM
facturas A INNER JOIN items_facturas B
ON A.NUMERO = B.NUMERO
WHERE A.FECHA_VENTA = vfecha1;
SELECT SUM(B.CANTIDAD * B.PRECIO) INTO facturacion_final  FROM
facturas A INNER JOIN items_facturas B
ON A.NUMERO = B.NUMERO
WHERE A.FECHA_VENTA = vfecha2;
SET variacion = ((facturacion_final / facturacion_inicial) -1) * 100;
CASE
WHEN variacion > 10 THEN SET mensaje = 'Verde';
WHEN variacion >= -10 AND variacion <= 10 THEN SET mensaje = 'Amarillo';
WHEN varacion < -10 THEN SET mensaje = 'Rojo';
END CASE;
SELECT mensaje;
END $$
```

### **Cantidad de facturas para diversos días**

Teniendo en cuenta que la siguiente función añade 1 día a la fecha:

```sql
SELECT ADDDATE(FECHA_VENTA, INTERVAL 1 DAY);
```

Crea un Stored Procedure que, a partir del día 01/01/2017, cuente el número de facturas hasta el día 10/01/2017. Debemos mostrar la fecha y el número de facturas día tras día. Llamaremos este SP como: suma_dias_facturas.

Tips:

Declara variables del tipo DATE: Fecha inicial y fecha final; Haz un loop para probar que la fecha inicial < fecha final; muestra en el output de MySQL la fecha y el número de facturas. No olvides convertir las variables en tipo VARCHAR; Suma la fecha en 1 día.

Rta.

```sql
DELIMITER $$
CREATE PROCEDURE `suma_dias_facturas`()
BEGIN
DECLARE fecha_inicial DATE;
DECLARE fecha_final DATE;
DECLARE n_facturas INT;
SET fecha_inicial = '20170101';
SET fecha_final = '20170110';
WHILE fecha_inicial <= fecha_final
DO
SELECT COUNT(*) INTO n_facturas  FROM facturas WHERE FECHA_VENTA = fecha_inicial;
SELECT concat(DATE_FORMAT(fecha_inicial, '%d/%m/%Y'), '-' , CAST(n_facturas AS CHAR(50))) AS RESULTADO;
SELECT ADDDATE(fecha_inicial, INTERVAL 1 DAY) INTO fecha_inicial;
END WHILE;
END $$
```

---

## **Cursor y función**

### **Limitante del comando SELECT INTO**

De las siguientes opciones ¿Cuál describe mejor la limitante del comando SELECT INTO?

Rta.

El comando SELECT INTO solamente es aplicable cuando la consulta devuelve una sola línea.

Un **Cursor** es una estructura implementada en MySQL que permite la interaccion linea a linea mediante un orden determinado.

### **Fases para uso del cursor**

- **Declaracion**: Definir la consulta que sera depositada en el cursor.
- **Abertura**: Abrimos el cursor para recorrerlo un linea a la vez.
- **Recorrido**: Linea por linea hasta el final.
- **Cierre**: Cerramos el cursor.
- **Limpiar**: Limpia el cursor de la memoria.

### **Hallando el valor total del crédito**

Crea un Stored Procedure usando un cursor para hallar el valor total de todos los créditos de todos los clientes. Llamaremos este SP como: limite_creditos.

Tips:

Declara dos variables: Una que recibe el límite de crédito del cursor y otra el límite de crédito total; haz un loop en el cursor y ve sumando en la variable límite de crédito total el valor del límite de cada cliente; Exhibe el valor total del límite.

Rta.

```sql
DELIMITER $$
CREATE PROCEDURE `limite_creditos`()
BEGIN
DECLARE limite_credito FLOAT;
DECLARE limite_credito_acumulado FLOAT;
DECLARE fin_cursor INT;
DECLARE c CURSOR FOR SELECT LIMITE_DE_CREDITO FROM tabla_de_clientes;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin_cursor = 1;
SET fin_cursor = 0;
SET limite_credito_acumulado = 0;
SET limite_credito = 0;
OPEN c;
WHILE fin_cursor = 0
DO
FETCH c INTO limite_credito;
IF fin_cursor = 0
THEN SET limite_credito_acumulado = limite_credito_acumulado + limite_credito;
END IF;
END WHILE;
SELECT limite_credito_acumulado;
CLOSE c;
END $$
```

### **Calculando el valor total de la facturación**

Crea un Stored Procedure usando un cursor para hallar el valor total de la facturación para un determinado mes y año.

Tips:

Declara tres variables: Una que recibe la cantidad, otra el precio y otra que va a acumular la facturación; Haz un loop en el cursor e ve sumando el valor de facturación en cada factura; Exhibe el valor total del límite; Recuerda que la consulta quiere obtener la facturación de un mes y año. El comando a continuación muestra todas las facturas generadas en enero de 2017:

```sql
SELECT IFa.CANTIDAD, IFa.PRECIO FROM items_facturas IFa
INNER JOIN facturas  F ON F.NUMERO = IFa.NUMERO
WHERE MONTH(F.FECHA_VENTA) = 1 AND YEAR(F.FECHA_VENTA) = 2017;
```

Llamaremos este Stored Procedure como: campo_adicional.

Rta.

```sql
DELIMITER $$
CREATE PROCEDURE `campo_adicional`()
BEGIN
DECLARE cantidad INT;
DECLARE precio FLOAT;
DECLARE facturacion_acumulada FLOAT;
DECLARE fin_cursor INT;
DECLARE c CURSOR FOR
SELECT IFa.CANTIDAD, IFa.PRECIO FROM items_facturas IFa
INNER JOIN facturas  F ON F.NUMERO = IFa.NUMERO
WHERE MONTH(F.FECHA_VENTA) = 1 AND YEAR(F.FECHA_VENTA) = 2017;
DECLARE CONTINUE HANDLER FOR NOT FOUND
SET fin_cursor = 1;
OPEN c;
SET fin_cursor = 0;
SET facturacion_acumulada = 0;
WHILE fin_cursor = 0
DO
FETCH c INTO cantidad, precio;
IF fin_cursor = 0 THEN
SET facturacion_acumulada = facturacion_acumulada + (cantidad * precio);
END IF;
END WHILE;
CLOSE c;
SELECT facturacion_acumulada;
END $$
```

### **Obteniendo el número de facturas**

Vea el Stored Procedure de abajo:

```sql
DELIMITER $$
CREATE PROCEDURE `sp_numero_facturas` ()
BEGIN
DECLARE n_facturas INT;
SELECT COUNT(*) INTO n_facturas FROM facturas WHERE FECHA_VENTA = '20170101';
SELECT n_facturas;
END $$
```

Transforma este SP en una función donde ingresamos como parámetro la fecha y retornamos el número de facturas. Llamaremos esta función f_numero_facturas. Luego de crear la función, ejecútalo utilizando el comando SELECT.

Rta.

```sql
DELIMITER $$
CREATE FUNCTION `f_numero_facturas`(fecha DATE)
RETURNS INTEGER
BEGIN
DECLARE n_facturas INT;
SELECT COUNT(*) INTO n_facturas FROM facturas WHERE FECHA_VENTA = fecha;
RETURN n_facturas;
END $$

SELECT f_numero_facturas() AS RESULTADO;
```
