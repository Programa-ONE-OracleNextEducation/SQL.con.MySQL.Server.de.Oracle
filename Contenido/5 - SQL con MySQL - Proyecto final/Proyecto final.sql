USE jugos_ventas;
SELECT * FROM tabla_de_clientes;

SELECT RAND();

/*
MIN = 20 Y MAX = 250
( RAND() * ( MAX - MIN + 1 ) ) + MIN
*/

SELECT ( RAND() * ( 250 - 20 + 1 ) ) + 20 AS ALEATORIO;

SELECT FLOOR( ( RAND() * ( 250 - 20 + 1 ) ) + 20 ) AS ALEATORIO;

DELIMITER $$
CREATE FUNCTION `f_aleatorio` (min INT, max INT)
RETURNS INTEGER
BEGIN
	DECLARE vresultado INT;
	SELECT FLOOR( ( RAND() * ( max - min + 1 ) ) + min ) INTO vresultado;
    RETURN vresultado;
END $$

SET GLOBAL log_bin_trust_function_creators = 1;

SELECT f_aleatorio(5, 20) AS RESULTADO;

/* Utilizando LIMIT */

SELECT * FROM tabla_de_clientes;

SELECT * FROM tabla_de_clientes LIMIT 5;
SELECT * FROM tabla_de_clientes LIMIT 0, 2;

/* CLIENTE ALEATORIO */

DELIMITER $$
CREATE FUNCTION `f_cliente_aleatorio` ()
RETURNS VARCHAR(11)
BEGIN
	DECLARE vresultado VARCHAR(11);
    DECLARE vmax INT;
    DECLARE valeatorio INT;
	SELECT COUNT(*) INTO vmax FROM tabla_de_clientes;
    SET valeatorio = f_aleatorio(1, vmax);
    SET valeatorio = valeatorio - 1;
    SELECT DNI INTO vresultado from tabla_de_clientes LIMIT valeatorio,1;
    RETURN vresultado;
END $$

SELECT f_cliente_aleatorio() AS CLIENTE;

/* VENDEDOR ALEATORIO */

DELIMITER $$
CREATE FUNCTION `f_vendedor_aleatorio`()
RETURNS varchar(5)
BEGIN
DECLARE vresultado VARCHAR(5);
DECLARE vmax INT;
DECLARE valeatorio INT;
SELECT COUNT(*) INTO vmax FROM tabla_de_vendedores;
SET valeatorio = f_aleatorio(1,vmax);
SET valeatorio = valeatorio-1;
SELECT MATRICULA INTO vresultado FROM tabla_de_vendedores LIMIT valeatorio,1;
RETURN vresultado;
END $$

SELECT f_vendedor_aleatorio() AS VENDEDOR;

/* PRODUCTO ALEATORIO */

DELIMITER $$
CREATE FUNCTION `f_producto_aleatorio`()
RETURNS varchar(10)
BEGIN
DECLARE vresultado VARCHAR(10);
DECLARE vmax INT;
DECLARE valeatorio INT;
SELECT COUNT(*) INTO vmax FROM tabla_de_productos;
SET valeatorio = f_aleatorio(1,vmax);
SET valeatorio = valeatorio-1;
SELECT CODIGO_DEL_PRODUCTO INTO vresultado FROM tabla_de_productos LIMIT valeatorio,1;
RETURN vresultado;
END $$

SELECT f_producto_aleatorio() AS PRODUCTO;

/* GENERANDO VENTA */

CREATE TABLE facturas(
NUMERO INT NOT NULL,
FECHA DATE,
DNI VARCHAR(11) NOT NULL,
MATRICULA VARCHAR(5) NOT NULL,
IMPUESTO FLOAT,
PRIMARY KEY (NUMERO),
FOREIGN KEY (DNI) REFERENCES tabla_de_clientes(DNI),
FOREIGN KEY (MATRICULA) REFERENCES tabla_de_vendedores(MATRICULA)
);

SELECT * FROM facturas;

CREATE TABLE items_facturas(
NUMERO INT NOT NULL,
CODIGO_DEL_PRODUCTO VARCHAR(10) NOT NULL,
CANTIDAD INT,
PRECIO FLOAT,
PRIMARY KEY (NUMERO, CODIGO_DEL_PRODUCTO),
FOREIGN KEY (NUMERO) REFERENCES facturas(NUMERO),
FOREIGN KEY (CODIGO_DEL_PRODUCTO) REFERENCES tabla_de_productos(CODIGO_DEL_PRODUCTO)
);

SELECT * FROM items_facturas;

DELIMITER $$
CREATE PROCEDURE `sp_venta`( fecha DATE, maxitems INT, maxcantidad INT )
BEGIN
DECLARE vcliente VARCHAR(11);
DECLARE vproducto VARCHAR(10);
DECLARE vvendedor VARCHAR(5);
DECLARE vcantidad INT;
DECLARE vprecio FLOAT;
DECLARE vitems INT;
DECLARE vnfactura INT;
DECLARE vcontador INT DEFAULT 1;
DECLARE vnumitems INT;
SELECT MAX(NUMERO) + 1 INTO vnfactura FROM facturas;
SET vcliente = f_cliente_aleatorio();
SET vvendedor = f_vendedor_aleatorio();
INSERT INTO facturas (NUMERO, FECHA_VENTA, DNI, MATRICULA, IMPUESTO)
VALUES (vnfactura, fecha, vcliente, vvendedor, 0.16);
SET vitems = f_aleatorio(1, maxitems);
WHILE vcontador <= vitems
DO
SET vproducto = f_producto_aleatorio();
SELECT COUNT(*) INTO vnumitems FROM items_facturas
WHERE CODIGO_DEL_PRODUCTO = vproducto AND NUMERO = vnfactura;
IF vnumitems = 0 THEN
	SET vcantidad = f_aleatorio(1, maxcantidad);
	SELECT PRECIO_DE_LISTA INTO vprecio FROM tabla_de_productos WHERE CODIGO_DEL_PRODUCTO = vproducto;
	INSERT INTO items_facturas(NUMERO, CODIGO_DEL_PRODUCTO, CANTIDAD, PRECIO)
	VALUES(vnfactura, vproducto, vcantidad, vprecio);
END IF;
SET vcontador = vcontador + 1;
END WHILE;
END $$

SELECT MAX(NUMERO) FROM facturas;

CALL sp_venta('20210619', 3, 100);
CALL sp_venta('20210619', 20, 100);

/* TRIGGER */

SELECT A.FECHA_VENTA, SUM(B.CANTIDAD * B.PRECIO) AS FACTURACION
FROM facturas A
INNER JOIN
items_facturas B
ON A.NUMERO = B.NUMERO
WHERE A.FECHA_VENTA = '20210619'
GROUP BY A.FECHA_VENTA;

/* TRIGGERS CON STORED PROCEDURE */

CREATE TABLE facturacion(
FECHA DATE NULL,
VENTA_TOTAL FLOAT
);

DROP TRIGGER TG_FACTURACION_INSERT;
DROP TRIGGER TG_FACTURACION_DELETE;
DROP TRIGGER TG_FACTURACION_UPDATE;

DELIMITER //
CREATE TRIGGER TG_FACTURACION_INSERT 
AFTER INSERT ON items_facturas
FOR EACH ROW BEGIN
  CALL sp_triggers;
END //

DELIMITER //
CREATE TRIGGER TG_FACTURACION_DELETE
AFTER DELETE ON items_facturas
FOR EACH ROW BEGIN
  CALL sp_triggers;
END //

DELIMITER //
CREATE TRIGGER TG_FACTURACION_UPDATE
AFTER UPDATE ON items_facturas
FOR EACH ROW BEGIN
  CALL sp_triggers;
END //

CALL sp_venta('20210622', 15, 100);

SELECT * FROM facturacion WHERE FECHA = '20210622';

DELIMITER $$
CREATE PROCEDURE `sp_triggers`()
BEGIN
  DELETE FROM facturacion;
  INSERT INTO facturacion
  SELECT A.FECHA_VENTA, SUM(B.CANTIDAD * B.PRECIO) AS VENTA_TOTAL
  FROM facturas A
  INNER JOIN
  items_facturas B
  ON A.NUMERO = B.NUMERO
  GROUP BY A.FECHA_VENTA;
END $$

