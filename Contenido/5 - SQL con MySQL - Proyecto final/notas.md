# **SQL con MySQL - Proyecto final**

## **Proyectando la base de datos**

### **Preparando el ambiente**

Aquí puedes descargar los archivos que necesitarás para iniciar con tu entrenamiento.

[Descargue los archivos en Github](https://github.com/alura-es-cursos/1834-proyecto-final-sql-con-mysql "GitHub") o haga [clic](https://github.com/alura-es-cursos/1834-proyecto-final-sql-con-mysql/archive/refs/heads/main.zip "folder.zip") aquí para descargarlos directamente.

### **Comando para crear la base de datos**

Considerando las siguientes opciones ¿Cuál de ellas emplea la sintaxis correcta para crear una base de datos en MySQL?

Rta.

```sql
CREATE DATABASE table_name;
CREATE SCHEMA table_name;
```

### **Comando para crear las tablas**

Considerando las siguientes opciones ¿Cuál de ellas emplea la sintaxis correcta para crear una tabla en MySQL?

Rta.

```sql
CREATE TABLE table_name (field_name datatype);
```

### **Insertar datos a las tablas**

¿Cuál de los siguientes comandos corresponde con la inserción de datos en las tablas?

Rta.

```sql
INSERT INTO table_name (field_name) VALUES (values);
INSERT table_name (field_name) VALUES (values);
```

### **Insertar datos en lote**

¿Cuál de los siguientes enunciados es verdadero?

1. Es posible insertar registros en una tabla provenientes de otra tabla hospedada en una base de datos externa siempre y cuando los nombres de los campos y los tipos de los campos sean idénticos y estos se incluyan en el mismo orden de la tabla de destino.
2. Es posible insertar registros en una tabla provenientes de otra tabla hospedada en una base de datos externa siempre y cuando la cantidad de campos seleccionados en la tabla de origen corresponda con la cantidad de campos requeridos con el comando INSERT.

Rta.

1. Falso. El nombre del campo no tiene que ser idéntico, de hecho, los campos ni siquiera necesitan ser del mismo tipo. Lo único que se requiere es que el número de campos en la cláusula INSERT corresponda con el número de campos en la cláusula SELECT.
2. Verdadero. Así es, lo único que se requiere es que el número de campos en la cláusula INSERT corresponda con el número de campos en la cláusula SELECT.

---

## **Función aleatorio**

### **La función RAND de MySQL**

La función RAND() muestra:

Rta.

Un número aleatorio entre 0 y 1. La función RAND() devuelve como resultado un número aleatorio en el intervalo [0;1].

### **Números aleatorios**

Observa el siguiente comando:

SELECT RAND() \* 8;

Rta.}

El output será un número en el intervalo [0;8]. Se trata de un número (con una altísima probabilidad de ser decimal) dentro del intervalo en mención.

### **Creando funciones en MySQL**

¿Cuál de los siguientes comandos permite la creación de funciones en MySQL?

Rta.

```sql
SET GLOBAL log_bin_trust_function_creators = 1;
```

---

## **Función cliente aleatorio**

### **Obteniendo un número entero de forma aleatoria**

Observa las siguientes líneas de comando:

```sql
CREATE FUNCTION `f_ejemplo`(min INTEGER, max INTEGER)
RETURNS INTEGER
BEGIN
  DECLARE vresultado INTEGER;
  SELECT(RAND() * (max-min+1))+min INTO vresultado;
RETURN vresultado;
END
```

¿Cómo se puede modificar a la función f_ejemplo para que nos devuelva un número entero?

Rta.

```sql
CREATE FUNCTION `f_ejemplo`(min INTEGER, max INTEGER)
RETURNS INTEGER
BEGIN
  DECLARE vresultado INTEGER;
  SELECT TRUNCATE((RAND() * (max-min+1))+min) INTO vresultado;
RETURN vresultado;
END
```

- La función **TRUNCATE()** devuelve como resultado un número decimal truncado a un número específico de casillas decimales, valga la redundancia (de acuerdo con los parámetros definidos).

```sql
CREATE FUNCTION `f_ejemplo`(min INTEGER, max INTEGER)
RETURNS INTEGER
BEGIN
  DECLARE vresultado INTEGER;
  SELECT CEIL((RAND() * (max-min+1))+min) INTO vresultado;
RETURN vresultado;
END
```

- La función **CEIL()** nos ayuda a obtener un número entero.

```sql
CREATE FUNCTION `f_ejemplo`(min INTEGER, max INTEGER)
RETURNS INTEGER
BEGIN
  DECLARE vresultado INTEGER;
  SELECT FLOOR((RAND() * (max-min+1))+min) INTO vresultado;
RETURN vresultado;
END
```

- La función **FLOOR()** nos ayuda a obtener un número entero.

### **Creando las funciones para producto y vendedor aleatorio**

En el video de esta aula creamos una función para obtener el cliente a través de la función de número aleatorio. En este ejercicio, crea otra función para obtener el producto y también el vendedor usando como base la función aleatorio.

Rta.

Para crear la función que genere un producto aleatorio, los comandos son los siguientes:

```sql
DELIMITER //
CREATE FUNCTION `f_producto_aleatorio`()
RETURNS varchar(10)
BEGIN
DECLARE vresultado VARCHAR(10);
DECLARE vmax INT;
DECLARE valeatorio INT;
SELECT COUNT(*) INTO vmax FROM productos;
SET valeatorio = f_aleatorio(1,vmax);
SET valeatorio = valeatorio-1;
SELECT CODIGO INTO vresultado FROM productos LIMIT valeatorio,1;
RETURN vresultado;
END //
```

Ahora bien, para generar el vendedor aleatorio, la función es:

```sql
DELIMITER //
CREATE FUNCTION `f_vendedor_aleatorio`()
RETURNS varchar(5)
BEGIN
DECLARE vresultado VARCHAR(5);
DECLARE vmax INT;
DECLARE valeatorio INT;
SELECT COUNT(*) INTO vmax FROM vendedores;
SET valeatorio = f_aleatorio(1,vmax);
SET valeatorio = valeatorio-1;
SELECT MATRICULA INTO vresultado FROM vendedores LIMIT valeatorio,1;
RETURN vresultado;
END //
```

---

## **Generando ventas y problema con PK**

### **Funciones y Stored Procedures**

Considera las siguientes afirmaciones:

1. La diferencia entre una función y una subrutina es que una función ejecuta una serie de comandos y devuelve un resultado; una subrutina ejecuta una serie de comandos pero no necesariamente devuelve un resultado.
2. Un stored procedure utiliza el comando CALL y una función utiliza el comando SELECT para su ejecución, respectivamente.

Rta.

Ambas afirmaciones son verdaderas.

### **Ciclos iterativos**

¿Cuál es la sintaxis correcta para utilizar el comando WHILE?

```sql
WHILE condition
DO
statements;
(...)
END WHILE;
```

Así es, de forma muy resumida, como se trabaja con ciclos iterativos utilizando el comando WHILE en MySQL.

### **La clave primaria**

¿Cuáles de las siguientes afirmaciones se aplican al concepto de clave primaria?

Rta.

- Puede existir más de una clave primaria en una misma tabla. La combinación de los campos que son clave primaria no se puede repetir en ningún otro registro de la tabla garantizando la integridad de los datos.
- La clave primaria ayuda a mantener la integridad de los datos al evitar duplicidad en los registros.

---

## **Stored Procedures y TRIGGERS**

### **Formato de facturación**

En la tabla de facturas tenemos el valor del impuesto. En la tabla de ítems tenemos la cantidad y la facturación. Calcula el valor del impuesto pago en el año de 2021 redondeando al mayor entero.

Rta.

```sql
SELECT YEAR(FECHA), CEIL(SUM(IMPUESTO * (CANTIDAD * PRECIO)))
AS RESULTADO
FROM facturas F
INNER JOIN items I ON F.NUMERO = I.NUMERO
WHERE YEAR(FECHA) = 2021
GROUP BY YEAR(FECHA);
```

### **Aplicando Stored Procedures a TRIGGERS**

¿Por qué genera más ventajas aplicar stored procedures a los TRIGGERS?

Rta.

Porque facilitan el mantenimiento. Si cambiamos las reglas de negocio, basta cambiarlas solamente en el sp sin necesidad de hacer alteraciones al interior de los TRIGGERS.
