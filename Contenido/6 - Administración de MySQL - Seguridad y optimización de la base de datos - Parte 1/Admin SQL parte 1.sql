USE jugos_ventas;

/* Crear tablas con el mecanismo de almacenamiento que le indique */

CREATE TABLE df_table (ID INT, NOMBRE VARCHAR(100));

ALTER TABLE df_table ENGINE = MyISAM;

CREATE TABLE df_table1 (ID INT, NOMBRE VARCHAR(100)) ENGINE = MEMORY;

SHOW ENGINES;

SHOW VARIABLES WHERE Variable_Name LIKE '%dir';

/* HACER UN BACK UP */

USE jugos_ventas;
/* Paramos la insercion de datos en la base de datos*/
LOCK INSTANCE FOR BACKUP;
/* 
	HACEMOS EL EXPORT 
*/
/* Desbloqueamos la insercion de datos en la base de datos*/
UNLOCK INSTANCE;

