# **Administración de MySQL - Seguridad y optimización de la base de datos - Parte 1**

## **Recuperación de la base de datos**

### **Preparando el ambiente**

Aquí puedes descargar los archivos que necesitarás para iniciar con tu entrenamiento.

[Descargue los archivos en Github](https://github.com/alura-es-cursos/1838-administracion-de-mysql-parte-1 "GitHub") o haga clic [aquí](https://github.com/alura-es-cursos/1838-administracion-de-mysql-parte-1/archive/refs/heads/main.zip "folder.zip") para descargarlos directamente.

### **Recuperación de la base**

Al acceder a Workbench, ¿En cuál pestaña encuentras la opción que te permite recuperar la base de datos?

Rta.

Administration. En esta pestaña podrás elegir la opción Data Import/Restore.

---

## **Papel del DBA**

### **Funciones del DBA**

¿Cuáles de las siguientes opciones son funciones del DBA (Database Administrator)?

Rta.

- Optimizar las consultas a los informes. Esta función del DBA está relacionada con el mejoramiento del desempeño.
- Instalar el software.

### **Información para las conexiones**

¿Cuáles parámetros componen la información principal para crear conexiones?

Rta.

Usuario, Servidor, Puerta y Contraseña.

### **Formas de detener y de reiniciar el servicio**

En Windows ¿Cuáles son las maneras de detener y de reiniciar el servicio como presentado en el video?

Rta.

A través de Servicios y del Símbolo del Sistema.

```sh
net stop mysql80
net start mysql80
```

---

## **Tuning y mecanismos de almacenamiento**

### **Puntos importantes del Tuning**

Sobre el Tuning de Hardware, ¿Cuáles de las siguientes opciones son verdaderas?

Rta.

- Ver la relación entre RAM y base de datos.
- Usar uma controladora de disco RAID.

### **Valor de la variable**

¿Cuál es el comando para ver el valor de una variable global?

Rta.

```sql
SHOW GLOBAL STATUS 'name_var_global';
```

### **Mecanismos de almacenamiento**

- La forma de almacenar la informacion en las tablas.
- MySQL 8.0 Community dispone de 9 Mecanismos para almacenar datos en las tablas. (Una misma DB puede usar diversos mecanismos en sus tablas).
- **ENGINE**: Es el parametro que indica el mecanismo de almacenamiento empleado en la tabla.
- Los 3 principales engines son: **MyISAM, InnoDB y MEMORY.**

### **Aplicación del mecanismo de almacenamiento**

¿Dónde aplicamos el mecanismo de almacenamiento?

Rta.

El mecanismo de almacenamiento es una propiedad de la tabla.

**MyISAM**

- No es transaccional. (No esta diseñado para que varios usuarios realicen alteraciones a las tablas simultaneamente)
- Solamente permite el bloqueo a nivel de tabla. (Lectura mas rapida)
- Recomendable para tablas que no estan cambiando continuamente.
- La clave externa no soporta Full Text.
- Almacena tablas de manera mas compacta. (Optimiza espacio de almacenamiento)
- Implementa indices HASH y BTREE. (Ventaja)

**InnonDB**

- Mecanismos de almacenamiento transaccional mas utilizado en MySQL. (Si esta diseñado para que varios usuarios realicen operaciones en las tablas simultaneamente).
- Soporte transaccional completo / Soporte a claves externas.
- Cache de buffer configurado de forma separada tanto para la base como para el indice.
- Bloqueo de tabla a nivel de linea.
- Indexacion BTREE.
- Back-up de DB online - Sin bloqueo.

**MEMORY**

- Mecanismo de almacenamiento que crea tablas en memoria RAM. No en disco.
- No soporta clave externa.
- Acceso muy rapido a la informacion.
- Los datos necesitan ser reinicializados junto con el servidor.
- Bloqueo a nivel de tabla.
- Indice utiliza HASH por defecto y BTREE.
- Formato de linea de longitud fija. (No soporta BLOB/TEXT)

### **Acceso a los datos**

Suponiendo que las estructuras que serán comparadas sean idénticas, ¿Cuál de los siguientes mecanismos de almacenamiento es el más rápido para acceder a los dados?

Rta.

El mecanismo de almacenamiento en memoria es el de más rápido acceso.

---

## **Usando mecanismos de almacenamiento y directorios**

### **Mecanismo de almacenamiento por defecto**

Si no modificamos ningún parámetro del ambiente original, ¿Cuál es el mecanismo de almacenamiento por defecto de la versión 8.0 de MySQL?

Rta.

InnoDB

### **Directorio de creación de la base de datos**

¿Cuál es la variable de ambiente que determina la localización para la creación de la base de datos?

Rta.

datadir es la variable que determina la localización de la base de datos.

---

## **Back-up de la base de datos**

Es una copia de seguridad de la base que se realiza periodicamente para recuperarla en caso de problemas con la base de datos.

Hay 2 tipos de back up.

**BackUp Logico**

- Exporta todas las estructuras, tablas, rutinas, (...) a un script sql que al ser ejecutado recrea la base de datos.
- Permite la manipulacion externa, antes de recuperar la informacion.
- Es mas lento porque se ejecuta comando a comando.

**BackUp Fisico**

- Contiene todos los archivos binarios del sistema donde la informacion esta almacenada, pero sin los respectivos comandos.
- Es mas rapido que el backup logico, pero es menos flexible. (No permite editar tablas y datos de su restauracion)

> mysqldump : para crear backup logicos.

- Dentro de c:\Program Files\MySQL\MySQL Server 8.0\bin ejecutar:

```sql
mysqldump
mysqldump -uroot -p --databases nombre_db > path_destino+nombre_file.sql
mysqldump -uroot -p --databases nombre_db --ignore-table nombre_db.nombre_table > path_destino+nombre_file-sin-nombre-table.sql
```

### **Backup de una base de datos**

Tengo una base llamada BANCODB, con las siguientes tablas:

- TABLA1
- TABLA2
- TABLA3

Queremos ejecutar un backup tan solo de las tablas TABLA1 y TABLA2. ¿Cuál sería el comando para ello?

Rta.

```sql
mysqldump -uroot -p --databases BANCODB --ignore-table BANCODB.TABLA3
```

### **Directorio con backup de las tablas**

¿Cuál es la opción que le permite a Workbench crear un subdirectorio e incluya un archivo de comandos SQL para cada tabla?

Rta.

Export do Dump Project Folder, aquí creamos un subdirectorio, y en su interior un archivo SQL para recuperar cada tabla.

### **Pasos para realizar el backup**

Para realizar el backup a través de archivos, seguí los siguientes pasos:

- Detuve el servicio de MySQL.
- Creé el directorio en el computador.
- Copié todo el directorio Data en el nuevo directorio creado anteriormente.
- Reinicié el servicio de MySQL.

Luego de ejecutar estos pasos, fueron realizados cambios en la base, creadas nuevas bases, eliminadas otras, y, cuando recuperamos el backup para deshacer toda la modificación, el procedimiento no fue exitoso.

¿Dónde está el error?

Rta.

No se copió el archivo my.ini. El archivo de configuración my.ini debe ser copiado para realizar el backup y sustituido al momento de hacer la recuperación.

### **Recuperar la BD**

**Logico**

- Dentro de c:\Program Files\MySQL\MySQL Server 8.0\bin ejecutar:

```sql
mysql
mysql -uroot -p > path_destino_file.sql
```

**Fisico**

Se hace el proceso inverso.

### **Programa para recuperar el archivo**

¿Cuál es la herramienta que usamos para recuperar una base de datos a través del Símbolo del Sistema?

Rta.

mysql. En realidad, no existe una herramienta específica para recuperar el backup. Tan solo, ejecutamos MySQL y el script con extensión .sql que fue generado por el proceso de backup.
