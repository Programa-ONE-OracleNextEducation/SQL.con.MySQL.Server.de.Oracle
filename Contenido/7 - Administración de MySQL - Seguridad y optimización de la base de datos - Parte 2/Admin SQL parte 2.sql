USE jugos_ventas;

/*----------------------------------------------------------------------------------------*/
/* PLAN DE EJECUCION */

-- Duracion: 0.000 seg
-- cost: 3.75
SELECT A.CODIGO_DEL_PRODUCTO FROM tabla_de_productos A;

-- Duracion: 0.031 seg
-- cost: 41947.04
SELECT A.CODIGO_DEL_PRODUCTO, C.CANTIDAD FROM tabla_de_productos A
INNER JOIN items_facturas C
ON A.CODIGO_DEL_PRODUCTO = C.CODIGO_DEL_PRODUCTO;

-- Duracion: 0.063 seg
-- cost: 138676.67
SELECT A.CODIGO_DEL_PRODUCTO, YEAR(FECHA_VENTA) AS ANO, C.CANTIDAD FROM tabla_de_productos A
INNER JOIN items_facturas C
ON A.CODIGO_DEL_PRODUCTO = C.CODIGO_DEL_PRODUCTO
INNER JOIN facturas B
ON C.NUMERO = B.NUMERO;

-- Duracion: 2.140 seg
-- cost: 138676.67
SELECT A.CODIGO_DEL_PRODUCTO, YEAR(FECHA_VENTA) AS ANO, SUM(C.CANTIDAD) AS CANTIDAD FROM tabla_de_productos A
INNER JOIN items_facturas C
ON A.CODIGO_DEL_PRODUCTO = C.CODIGO_DEL_PRODUCTO
INNER JOIN facturas B
ON C.NUMERO = B.NUMERO
GROUP BY A.CODIGO_DEL_PRODUCTO, YEAR(FECHA_VENTA)
ORDER BY A.CODIGO_DEL_PRODUCTO, YEAR(FECHA_VENTA);

/*
GROUP BY y ORDER BY NO TIENE COSTO.
JOIN SI GENERA MAS COSTO.
OBSERVACION: ver que el costo para las ultimas consultas es igual. Uno tiene GROUP BY y ORDER BY y el otro no. El JOIN
es el mismo. Por lo tanto la coincide con la afirmacion al inicio.
*/

/*----------------------------------------------------------------------------------------*/
/* INDICES */

-- Duracion: 0.109 seg
-- cost: 8849.05
SELECT * FROM facturas WHERE FECHA_VENTA="20170101";

-- ADD INDEX - De esta forma agregamos un indice a la tabla.
ALTER TABLE facturas ADD INDEX(FECHA_VENTA);

-- El nuevo costo luego de agregar el indice
-- Duracion: 0.046 seg
-- cost: 54.05
SELECT * FROM facturas WHERE FECHA_VENTA="20170101";

-- DROP INDEX - De esta forma Eliminamos un indice a la tabla.
ALTER TABLE facturas DROP INDEX FECHA_VENTA;

/*----------------------------------------------------------------------------------------*/
/* TRABAJANDO CON INDICES Y MYSQLSLAP */

CREATE TABLE `facturas1` (
  `DNI` varchar(11) NOT NULL,
  `MATRICULA` varchar(5) NOT NULL,
  `FECHA_VENTA` date DEFAULT NULL,
  `NUMERO` int NOT NULL,
  `IMPUESTO` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `items_facturas1` (
  `NUMERO` int NOT NULL,
  `CODIGO_DEL_PRODUCTO` varchar(10) NOT NULL,
  `CANTIDAD` int NOT NULL,
  `PRECIO` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `tabla_de_productos1` (
  `CODIGO_DEL_PRODUCTO` varchar(10) NOT NULL,
  `NOMBRE_DEL_PRODUCTO` varchar(50) DEFAULT NULL,
  `TAMANO` varchar(10) DEFAULT NULL,
  `SABOR` varchar(20) DEFAULT NULL,
  `ENVASE` varchar(20) DEFAULT NULL,
  `PRECIO_DE_LISTA` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO facturas1
SELECT * FROM facturas;

INSERT INTO items_facturas1
SELECT * FROM items_facturas;

INSERT INTO tabla_de_productos1
SELECT * FROM tabla_de_productos;

-- Ejecutamos los mismo comando pero esta vez sin las claves primarias ni foraneas.

-- Duracion: 0.000 seg
-- cost CON PK y FK: 3.75
-- cost SIN PK y FK: 3.75
SELECT A.CODIGO_DEL_PRODUCTO FROM tabla_de_productos1 A;

-- Duracion: 0.031 seg
-- cost CON PK y FK: 41947.04
-- cost SIN PK y FK: 746621.42
SELECT A.CODIGO_DEL_PRODUCTO, C.CANTIDAD FROM tabla_de_productos1 A
INNER JOIN items_facturas1 C
ON A.CODIGO_DEL_PRODUCTO = C.CODIGO_DEL_PRODUCTO;

-- Duracion: 0.063 seg
-- cost CON PK y FK: 138676.67
-- cost SIN PK y FK: 6553019711.53 (6 Billones)
SELECT A.CODIGO_DEL_PRODUCTO, YEAR(FECHA_VENTA) AS ANO, C.CANTIDAD FROM tabla_de_productos1 A
INNER JOIN items_facturas1 C
ON A.CODIGO_DEL_PRODUCTO = C.CODIGO_DEL_PRODUCTO
INNER JOIN facturas1 B
ON C.NUMERO = B.NUMERO;

-- Duracion: 2.140 seg
-- cost CON PK y FK: 138676.67
-- cost SIN PK y FK: 6553019711.53 (6 Billones)
SELECT A.CODIGO_DEL_PRODUCTO, YEAR(FECHA_VENTA) AS ANO, SUM(C.CANTIDAD) AS CANTIDAD FROM tabla_de_productos1 A
INNER JOIN items_facturas1 C
ON A.CODIGO_DEL_PRODUCTO = C.CODIGO_DEL_PRODUCTO
INNER JOIN facturas1 B
ON C.NUMERO = B.NUMERO
GROUP BY A.CODIGO_DEL_PRODUCTO, YEAR(FECHA_VENTA)
ORDER BY A.CODIGO_DEL_PRODUCTO, YEAR(FECHA_VENTA);

/*
CONCLUSION : El hecho de no tener clave primarias ni foraneas va a perjudicar en el desempeño de las consultas (mayor costo). 
*/

/*----------------------------------------------------------------------------------------*/
/* GESTION DE USUARIOS */

-- crea un usuario COMPLETO.
CREATE USER 'admin02'@'localhost' IDENTIFIED BY 'admin02';
-- asigna los privilegios (completo) al usuario creado.
GRANT ALL PRIVILEGES ON *.* TO 'admin02'@'localhost' WITH GRANT OPTION;

-- crea un usuario NORMAL.
CREATE USER 'user02'@'localhost' IDENTIFIED BY 'user02';
-- asigna los privilegios al usuario creado.
GRANT SELECT, INSERT, UPDATE, DELETE,
CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE -- EXECUTE hace referencia al STORED PRODEDURE
ON *.* TO 'user02'@'localhost' WITH GRANT OPTION;

-- crea un usuario de SOLO LECTURA.
CREATE USER 'read01'@'localhost' IDENTIFIED BY 'read01';
-- asigna los privilegios al usuario creado.
GRANT SELECT, EXECUTE ON *.* TO 'read01'@'localhost' WITH GRANT OPTION;

-- crea un usuario para BACKUP.
CREATE USER 'back01'@'localhost' IDENTIFIED BY 'back01';
-- asigna los privilegios al usuario creado.
GRANT SELECT, RELOAD, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'back01'@'localhost' WITH GRANT OPTION;

/*----------------------------------------------------------------------------------------*/
/* GESTION DE PRIVILEGIOS */

-- crea un usuario NORMAL para acceso a una base de datos particular.
CREATE USER 'user03'@'localhost' IDENTIFIED BY 'user03';
-- asigna los privilegios al usuario creado.
GRANT SELECT, INSERT, UPDATE, DELETE,
CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE -- EXECUTE hace referencia al STORED PRODEDURE
ON jugos_ventas.* TO 'user03'@'localhost' WITH GRANT OPTION; -- *.* el 1er * es la base de datos y el 2do * es la tabla. * indica todo.

-- crea un usuario NORMAL para acceso a una base de datos y tabla particular.
CREATE USER 'user04'@'localhost' IDENTIFIED BY 'user04';
-- asigna los privilegios al usuario creado.
GRANT SELECT, INSERT, UPDATE, DELETE ON jugos_ventas.facturas TO 'user04'@'localhost' WITH GRANT OPTION; -- *.* el 1er * es la base de datos y el 2do * es la tabla. * indica todo.
GRANT SELECT ON jugos_ventas.tabla_de_vendedores TO 'user04'@'localhost' WITH GRANT OPTION; -- *.* el 1er * es la base de datos y el 2do * es la tabla. * indica todo.

-- ver los privilegios de todos los usuario
SELECT * FROM mysql.user;

-- ver los privilegios de un usuario en particular
SHOW GRANTS FOR 'user02'@'localhost';

-- eliminar todos los privilegios
REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'user02'@'localhost'