# **Administración de MySQL - Seguridad y optimización de la base de datos - Parte 2**

## **Plan de ejecución**

### **Preparando el ambiente**

Para que puedas continuar con este entrenamiento, debes de haber concluído la **Parte 1** del curso **Administración de MySQL: Seguridad y optimización de la base de datos**. Si aún no lo has hecho, te invito a que lo hagas antes de continuar con la siguiente actividad.

[Descargue los archivos en Github](https://github.com/alura-es-cursos/1839-administracion-de-mysql-parte-2 "GitHub") o haga clic [aquí](https://github.com/alura-es-cursos/1839-administracion-de-mysql-parte-2/archive/refs/heads/main.zip "folder.zip") para descargarlos directamente.

### **La consulta más lenta**

Analiza las siguientes consultas:

**1.**

```sql
SELECT * FROM TABLA1 INNER JOIN TABLA2 ON TABLA1.ID = TABLA2.ID INNER JOIN TABLA3 ON TABLA2.ID = TABLA3.ID
GROUP BY 2 ORDER BY TABLA1.ID;
```

**2.**

```sql
SELECT * FROM TABLA1 INNER JOIN TABLA2 ON TABLA1.ID = TABLA2.ID INNER JOIN TABLA3 ON TABLA2.ID = TABLA3.ID;
```

**3.**

```sql
SELECT * FROM TABLA1 INNER JOIN TABLA2 ON TABLA1.ID = TABLA2.ID;
```

¿Cuál de estas consultas se va a demorar más durante su ejecución?

Rta.

Consulta 1. Además de contar con 2 INNER JOINS realiza un GROUP BY a los datos.

### **Visualizando el plan de ejecución**

1. Abrir el cmd y dirijirse a c:\program files\MySQL\MySQL Server 8.0\bin y ejecutar **mysql -uroot -p**
2. Ejecutar
   - USE jugos_ventas;
   - EXPLAIN FORMAT = JSON SELECT A.CODIGO_DEL_PRODUCTO FROM tabla_de_productos A \G;
3. Ver el atributo **query_cost**, eso me indica el costo de la consulta.

### **El costo de la consulta**

¿Existe una medida que representa el costo de la consulta?

Rta.

No hay una unidad de medida. El costo no está expresado en ninguna unidad. Tan solo usamos este valor para compararlo con otros planes de ejecución. Cuanto menor sea el valor, más rápida va a ser la consulta.

---

## **Índices**

- Faclita la busqueda de datos dentro de la tabla. (Diccionario)
- Sin estructura de indices -> recorre la tabla hasta encontrar el registro.

- **MyISAM**

  - Crea una estructura separada para indices PK y no PK.
  - La columna del indice es ordenada y toma como referencia la posicion de la fila de la tabla original.
  - Implementa indices HASH y B-TREE

- **InnoDB**
  - La tabla original ya es ordenada con la PK.
  - Los indices que no son PK poseen estructura separadas y toman como referencia el valor de la PK.
  - Solo trabaja con B-Tree.

**_Hash y B-Tree son algoritmos de busqueda en listas ordenadas._**

- **B-Tree**
  - Arbol binario
  - Balanced - Binary Tree (Nodos distribuidos de forma balanceada)
  - +4 Billones de registros en 32 niveles.

### **El costo de tener un índice**

¿Cómo podría perjudicar a una base de datos el hecho de que una tabla contenga muchos índices?

Rta.

Muchos índices harán que los comandos de INSERT, UPDATE y DELETE se vuelvan más lentos. Como los índices deben ser reconstruidos cada vez que se modifican los datos de las tablas, muchos índices sí afectan el desempeño.

### **Indexación en las tablas**

¿Cuál es el mecanismo de almacenamiento en las tablas MySQL que puede trabajar con HASH y B-Tree?

Rta.

MyISAM. Las tablas MyISAM ofrecen soporte para HASH y B-Tree.

### **Algoritmo de búsqueda**

¿Cuál es el algoritmo que genera un árbol binario balanceado?

Rta.

El algoritmo B-Tree.

```sql
ALTER TABLE facturas ADD INDEX(FECHA_VENTA);
ALTER TABLE facturas DROP INDEX FECHA_VENTA;
```

### **Índices para mejorar una consulta**

Observa la siguiente consulta:

```sql
SELECT A.ID, B.ID, A.NOMBRE, B.NOMBRE FROM TABLA A
INNER JOIN TABLA B WHERE A.ID = B.ID
WHERE B.NOMBRE = 'JUAN';
```

Considerando que las tablas anteriores no tienen claves primarias ni externas.

¿Cuáles índices debemos construir para mejorar esta consulta?

Rta.

```sql
TABLA A ID
TABLA B ID
TABLA B NOMBRE
```

Usamos índices para los campos que hacen parte de la igualdad de los JOIN y el que está en el criterio de filtro WHERE.

---

## **Trabajando con índices y Mysqlslap**

### **El gráfico del plan de ejecución**

Cuando vemos un rectangulo de color verde en el plan de ejecución, ¿Qué significa?

Rta.

Que aquel Table Scan está usando un índice.

### **Mysqlslap**

Nos ayuda a simular multiples conexiones/clientes consumiendo recursos al mismo tiempo.

1. Abrir el cmd y dirijirse a c:\program files\MySQL\MySQL Server 8.0\bin y ejecutar
   - **mysqlslap -uroot -p --concurrency=100 --iterations=10 --create-schema=jugos_ventas -- query="SELECT \* FROM facturas WHERE FECHA_VENTA='20170101'";**
2. Hacemos lo mismo pero con el siguiente comando
   - **mysqlslap -uroot -p --concurrency=100 --iterations=10 --create-schema=jugos_ventas -- query="SELECT \* FROM facturas1 WHERE FECHA_VENTA='20170101'";**

Notemos que la primer consulta sobre la tabla facturas, el cual tiene un indice. el costo de simulacion es inferior al costo sobre la tabla facturas1 el cual no tiene PK, FK ni indice.

### **Creando los índices**

Retomando la siguiente consulta:

SELECT A.ID, B.ID, A.NOMBRE, B.NOMBRE FROM TABLA A
INNER JOIN TABLA B WHERE A.ID = B.ID
WHERE B.NOMBRE = 'JUAN';

Considerando que las tablas anteriores sí tienen claves primarias.

¿Cuáles índices debemos construir para mejorar esta consulta?

Rta.

```txt
TABLA A ID
TABLA B ID
TABLA B NOMBRE
```

INCORRECTO. Los campos de ID en ambas tablas, por ser claves primarias, ya tienen sus índices creados automáticamente.

```txt
TABLA B NOMBRE
```

CORRECTO. Como los campos de ID son claves primarias, entonces únicamente habría que crear índices para el campo que contiene el criterio de filtro a través de la cláusula WHERE.

---

## **Gestión de usuarios**

Creacion de un usuario con todos los privilegios con comandos sql.

```sql
-- crea un usuario.
CREATE USER 'admin02'@'localhost' IDENTIFIED BY 'admin02';
-- asigna los privilegios al usuario creado.
GRANT ALL PRIVILEGES ON *.* TO 'admin02'@'localhost' WITH GRANT OPTION;
```

### **Excluir el usuario root**

¿Por qué es aconsejable excluir el usuario root?

Rta.

Porque este usuario es creado durante la instalación y su contraseña es definida en ese momento, por el operador que realizó la instalación. El usuario root es creado durante la instalación y su contraseña es definida por un operador que hizo la instalación. Pero, no siempre, está persona será el DBA. Cambiar la contraseña del usuario root también resuelve el problema de seguridad, pero lo aconsejable es crear un nuevo usuario administrador y excluir el root.

### **Propiedades del usuario normal**

¿Cuáles son los privilegios del usuario normal?

Rta.

```sql
SELECT, INSERT, UPDATE, DELETE, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE
```

Estos son los privilegios que se deben asignar a un usuario normal para que pueda trabajar en la base de datos.

### **Privilegios READ y EXECUTE**

Al habilitar los privilegios de READ y EXECUTE al usuario de lectura, ¿Qué sucede cuando este ejecuta un Stored Procedure que realiza mantenimiento en las tablas?

Rta.

El Stored Procedure será ejecutado normalmente, aunque el usuario tenga solamente la propiedad READ para las tablas. Esta es una característica peligrosa, porque a pesar de que el usuario no tenga los privilegios de INSERT, UPDATE y DELETE, los comandos que realizan estas ejecuciones en la tabla, que se encuentran al interior del Stored Procedure, serán ejecutados, porque la característica EXECUTE se sobrepone a los mismos.

### **Privilegios para hacer el backup**

¿Cuáles son los privilegios para que el usuario pueda realizar los backups?

Rta.

```sql
SELECT, RELOAD, LOCK TABLES, REPLICATION CLIENT
```

Estas son las propiedades necesarias para que un usuario con privilegios de backup, pueda ejecutar el mismo en la base de datos.

---

## **Gestión de privilegios**

### **Accediendo desde cualquier servidor**

```sql
-- crea un usuario.
CREATE USER 'admingeneric02'@'%' IDENTIFIED BY 'admingeneric02';
-- asigna los privilegios al usuario creado.
GRANT ALL PRIVILEGES ON *.* TO 'admingeneric02'@'%' WITH GRANT OPTION;
```

El **%** indica que ese usuario (admingeneric02) puede acceder a la base de datos desde cualquier computadora (direccion IP).

### **Otras formas de usar %**

- **192.168.1.%** => 192.168.1.0 - 192.168.1.255
- **192.168.1.1\_\_** => 192.168.1.100 - 192.168.1.155
- **client\_\_.mycompany.com** => clientXY.mycompany.com

### **Intervalo de direcciones IP**

¿Cuál será el intervalo de direcciones IP si configuramos, con los comandos de privilegio de usuario, el valor 192.168.%?

Rta.

192.168.0.0 a 192.168.255.255. Este es el grupo de direcciones IP que compone el intervalo presentado.

ver los premisos de todos los usuario\*\*

```sql
SELECT * FROM mysql.user;
```

ver los privilegios de todos los usuario

```sql
SELECT * FROM mysql.user;
```

ver los privilegios de un usuario en particular

```sql
SHOW GRANTS FOR 'user02'@'localhost';
```

eliminar todos los privilegios

```sql
REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'user02'@'localhost'
```
