## **Realice este curso para Base de Datos y:**

- Conozca cuál es el papel del DBA
- Realice un tuning de su hardware
- Efectúe un tuning del software modificando algunas variables de entorno
- Analice las características sobre los principales mecanismos de almacenamiento
- Aprenda cómo ejecutar correctamente el backup

### **Aulas**

- **Recuperación de la base de datos**

  - Presentación
  - Preparando el ambiente
  - Preparando el ambiente
  - Recuperando la base de datos
  - Recuperación de la base
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Papel del DBA**

  - Proyecto del aula anterior
  - Papel del DBA
  - Funciones del DBA
  - Conexiones de MySQL
  - Información para las conexiones
  - Servicio de MySQL
  - Formas de detener y de reiniciar el servicio
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Tuning y mecanismos de almacenamiento**

  - Proyecto del aula anterior
  - Tuning de Hardware
  - Puntos importantes del Tuning
  - Variables de ambiente
  - Valor de la variable
  - Mecanismos de almacenamiento
  - Aplicación del mecanismo de almacenamiento
  - InnoDB y MEMORY
  - Acceso a los datos
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Usando mecanismos de almacenamiento y directorios**

  - Proyecto del aula anterior
  - Usando mecanismos de almacenamiento
  - Mecanismo de almacenamiento por defecto
  - Creando la base de datos
  - Comando para la creación de la base de datos
  - Cambiando el directorio de la base de datos
  - Directorio de creación de la base de datos
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Back-up de la base de datos**

  - Proyecto del aula anterior
  - Back up con mysqldump
  - Backup de una base de datos
  - Back up con Workbench
  - Directorio con backup de las tablas
  - Back up físico
  - Pasos para realizar el backup
  - Recuperando Back ups
  - Programa para recuperar el archivo
  - Haga lo que hicimos en aula
  - Proyecto final
  - Lo que aprendimos
  - Conclusión
