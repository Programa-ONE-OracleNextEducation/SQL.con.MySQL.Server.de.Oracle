## **Realice este curso para Base de Datos y:**

- Aprenda a usar el plan de ejecución
- Conozca los algoritmos de indexación HASH y B-Tree
- Utilice los índices para mejorar el desempeño de sus consultas
- Administre usuarios y privilegios

### **Aulas**

- **Plan de ejecución**

  - Presentación
  - Preparando el ambiente
  - Plan de ejecución
  - La consulta más lenta
  - Visualizando el plan de ejecución
  - El costo de la consulta
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Índices**

  - Proyecto del aula anterior
  - Concepto de índices
  - El costo de tener un índice
  - Algoritmo B-Tree
  - Indexación en las tablas
  - Algoritmo HASH
  - Algoritmo de búsqueda
  - Usando índices
  - Índices para mejorar una consulta
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Trabajando con índices y Mysqlslap**

  - Proyecto del aula anterior
  - Utilizando Workbench
  - El gráfico del plan de ejecución
  - Mysqlslap
  - Creando los índices
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Gestión de usuarios**

  - Proyecto del aula anterior
  - Creando usuario administrador
  - Excluir el usuario root
  - Creando usuario normal
  - Propiedades del usuario normal
  - Creando usuario para solo lectura
  - Privilegios READ y EXECUTE
  - Creando usuario para backup
  - Privilegios para hacer el backup
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Gestión de privilegios**

  - Proyecto del aula anterior
  - Accediendo desde cualquier servidor
  - Intervalo de direcciones IP
  - Limitando acceso a los esquemas
  - Revocando privilegios
  - Haga lo que hicimos en aula
  - Proyecto final
  - Lo que aprendimos
  - Conclusión
