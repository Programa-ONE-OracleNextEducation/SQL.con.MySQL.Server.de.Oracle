## **Realice este curso para Base de Datos y:**

- Aprenda más sobre el modelaje de una base de datos relacional
- Cree tablas y relaciones entre ellas con FKs
- Utilicediversos métodos de importación para alimentar la base de datos
- Use COMMIT y ROLLBACK para garantizar la integridad de tus datos
- Trabaje con TRIGGERs y defina secuencias

### **Aulas**

- **Modelaje de la base de datos**

  - Presentación
  - Preparando el ambiente
  - Instalando MySQL
  - Revisión sobre entidades
  - La entidad básica
  - Revisión sobre tipos de datos
  - La clave primaria
  - Modelaje
  - Requisitos de una base de datos
  - Pasos importantes para la creación de un modelo conceptual
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Creando la estructura de la base de datos**

  - Proyecto del aula anterior
  - Creación de la base de datos
  - Parámetro para la creación de la base de datos
  - Creación de tabla con PK
  - Creación de la tabla de clientes
  - Creación de tabla por asistente
  - Creación de la tabla de facturas
  - Creación de tabla con FK
  - Creando claves externas
  - Renombrando la tabla
  - Cambiando el nombre de la tabla
  - Finalizando la creación de base de datos
  - Creando las tablas de productos y de vendedores
  - Diagrama
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Incluyendo datos en las tablas**

  - Proyecto del aula anterior
  - Incluyendo datos
  - Inclusión de registros en las tablas
  - Incluyendo multiples registros
  - Inclusión de registros a partir de otra tabla
  - Incluyendo registros con el asistente
  - Importando datos de archivos externos
  - Los campos
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Alterando y excluyendo datos existentes en las tablas**

  - Proyecto del aula anterior
  - Incluyendo datos
  - Alterando datos en la tabla de clientes
  - Usando UPDATE con FROM
  - Modificando el volumen de compra
  - Excluyendo datos de la tabla
  - Excluyendo facturas
  - Alterando y borrando toda la tabla
  - Cuidado al alterar o excluir datos
  - COMMIT y ROLLBACK
  - Utilizando ROLLBACK 2 veces
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Autoincremento, patrones y triggers**

  - Proyecto del aula anterior
  - Comando AUTO_INCREMENT
  - Valor de la secuencia
  - Definiendo patrones para campos
  - Comportamiento de un campo DEFAULT al utilizar INSERT
  - TRIGGERS #1
  - TRIGGERS #2
  - Calculando la edad del cliente
  - TRIGGER con UPDATE y DELETE
  - Comandos DML y TRIGGERs
  - Manipulación de datos
  - Otras formas de manipulación de datos
  - Haga lo que hicimos en aula
  - Proyecto final
  - Lo que aprendimos
  - Conclusión
