## **Realice este curso para Base de Datos y:**

- Acota tus datos con LIMIT
- Filtra consultas con HAVING
- Entiende la diferencia entre LEFT y RIGHT JOIN
- Aprende a usar Sub-Queries
- Utiliza funciones de MySQL
- Agrupa datos con GROUP BY
- Clasifica tus registros
- Aprende a generar informes realizando consultas avanzadas

### **Aulas**

- **Configurando el ambiente y conociendo SQL**

  - Presentación
  - Preparando el ambiente
  - Preparando el ambiente
  - Historia de SQL
  - Ventajas del lenguaje SQL
  - Historia de MySQL
  - Longevidad de SQL
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Filtrando las consultas de los datos**

  - Proyecto del aula anterior
  - Conociendo la base de datos
  - Visualizando el esquema de datos
  - Revisando consultas
  - Listando los datos de una tabla
  - Consultas condicionales
  - Resuelve la estructura lógica
  - Aplicando consultas condicionales
  - Seleccionando ventas
  - Usando LIKE
  - Buscando clientes
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Presentación de los datos de una consulta**

  - Proyecto del aula anterior
  - Usando DISTINCT
  - Barrios de Ciudad de México
  - Usando LIMIT
  - Observando una muestra de datos
  - Usando ORDER BY
  - Mayores ventas
  - Usando GROUP BY
  - Número de ventas
  - Usando HAVING
  - Clientes que realizaron compras en 2016
  - Usando CASE
  - Clasificando el número de ventas
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Uniendo tablas y consultas**

  - Proyecto del aula anterior
  - Usando JOINS
  - Obteniendo la facturación anual
  - Ejemplos de LEFT y RIGHT JOIN
  - Seleccionando el tipo de JOIN
  - Ejemplos de FULL y CROSS JOIN
  - Nueva selección del tipo de JOIN
  - Uniendo consultas
  - Diferencias entre UNION y UNION ALL
  - Subconsultas
  - Relación entre HAVING y Subconsulta
  - Views
  - Características de la vista
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Funciones de MySQL**

  - Proyecto del aula anterior
  - Funciones con STRINGS
  - Listando la dirección completa
  - Funciones de fecha
  - Edad de los clientes
  - Funciones matemáticas
  - Formato de facturación
  - Convirtiendo datos
  - Listando con expresión natural
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Ejemplos de informes**

  - Proyecto del aula anterior
  - Informe de ventas válidas #1
  - Realizando una consulta al informe
  - Informe de ventas válidas #2
  - Realizando una nueva consulta al informe
  - Informe de ventas por sabor
  - Ventas porcentuales por tamaño
  - Haga lo que hicimos en aula
  - Proyecto final
  - Lo que aprendimos
  - Conclusión
