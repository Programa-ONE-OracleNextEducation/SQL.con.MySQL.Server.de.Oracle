## **Realice este curso para Base de Datos y:**

- Aprenda a instalar y acceda a la base de datos MySQL
- Aprenda a ejecutar MySQL usando Workbench o el símbolo del sistema (command prompt)
- Estructura tus tablas con CREATE TABLE y ALTER TABLE
- Agrega datos a la base con INSERT
- Actualiza y elimina los datos con UPDATE y DELETE
- Realiza consultas con SELECT e filtra con WHERE

### **Aulas**

- **Instalando y configurando MySQL**

  - Presentación
  - Preparando el ambiente
  - Historia de SQL
  - Un poco más sobre SQL
  - Historia de MySQL
  - Un poco más sobre MySQL
  - Instalando MySQL Server
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Manipulando la base de datos**

  - Proyecto del aula anterior
  - Definiciones
  - Localización de la tabla
  - Conociendo Workbench
  - Componentes de las tablas
  - Creando una base de datos
  - Agrupando las tablas
  - Creando una base de datos usando el asistente
  - Comando para crear una base de datos
  - Eiminando una base de datos
  - Creando una base de datos
  - MySQL por línea de comando
  - Eliminación de una base de datos
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Administrando las tablas de la base de datos**

  - Proyecto del aula anterior
  - Tipos de datos
  - Tipo fecha
  - Creando la primera tabla
  - Mejor tipo de variable
  - Creando la tabla con el asistente
  - Creando tabla de vendedores
  - Eliminando las tablas
  - Eliminando tabla de vendedores
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Mantenimiento de los datos en las tablas**

  - Proyecto del aula anterior
  - Insertando registros en la tabla
  - Descargar el archivo
  - Incluyendo el primer vendedor
  - Insertando varios registros en la tabla
  - Incluyendo dos vendedores más
  - Alterando registros
  - Actualizando la información sobre los vendedores
  - Excluyendo registros
  - Eliminando un vendedor
  - Incluyendo la clave primaria
  - Importancia de la clave primaria
  - Manipulando fechas y campos lógicos
  - Modificando la tabla de vendedores
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Consultando los datos**

  - Proyecto del aula anterior
  - Incluyendo datos en la tabla
  - Descargar el archivo
  - Seleccionando a todos los vendedores
  - Filtrando registros
  - Seleccionando a un vendedor
  - Filtrando usando mayor que menor que y diferente
  - Seleccionando vendedores por el valor de la comisión
  - Filtrando fechas
  - Seleccionando a un vendedor por fecha
  - Filtros compuestos
  - Selección compuesta
  - Seleccionar a todos los vendedores
  - Haga lo que hicimos en aula
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
