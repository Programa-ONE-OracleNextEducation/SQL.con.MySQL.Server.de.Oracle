## **Realice este curso para Base de Datos y:**

- Aprenda a implementar Stored Procedures
- Use los Cursores para buscar datos
- Controle el flujo de sus Procedures
- Trate los errores correctamente
- Cree funciones en MySQL

### **Aulas**

- Preparando el Ambiente

  - Presentación
  - Preparando el ambiente
  - Preparando el ambiente
  - Recuperando la base de datos
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Stored Procedures básico**

  - Proyecto del aula anterior
  - Conceptos básicos
  - Cambio del Delimitador
  - Creando los primeros Stored Procedures
  - Estructura para crear los Stored Procedures
  - Alterando y excluyendo Stored Procedures
  - Comando para alterar un Stored Procedure
  - Declarando Variables
  - Estructura de comando para la creación de un Stored Procedure
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Interacciones con la base de datos**

  - Proyecto del aula anterior
  - Manipulando la base de datos
  - Actualizando la edad
  - Parámetros
  - Actualizando la comisión
  - Control de errores
  - Comando para tratar los errores
  - Atribución de valor usando SELECT
  - Usando SELECT para atribuir valores
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Control de flujo**

  - Proyecto del aula anterior
  - IF THEN ELSE
  - Evaluando la cantidad de facturas
  - IF THEN ELSEIF
  - Facturación anual
  - CASE END CASE
  - CASE NOT FOUND
  - CASE condicional
  - Facturación anual usando CASE condicional
  - Looping
  - Cantidad de facturas para diversos días
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Cursor y función**

  - Proyecto del aula anterior
  - Problema con SELECT INTO
  - Limitante del comando SELECT INTO
  - Definición de CURSOR
  - Looping con CURSOR
  - Hallando el valor total del crédito
  - CURSOR accediendo a más de un campo
  - Calculando el valor total de la facturación
  - Funciones
  - Obteniendo el número de facturas
  - Haga lo que hicimos en aula
  - Proyecto final
  - Lo que aprendimos
  - Conclusión
