## **Realice este curso para Base de Datos y:**

- Desarrolle un proyecto práctico utilizando MySQL
- Cree una base de datos con sus entidades y relaciones
- Manipule datos a través de comandos DML
- Realice consultas SQL a los datos
- Aprenda a trabajar con la función RAND() y desarrolla funciones
- Trabaje con Stored Procedures y TRIGGERS

### **Aulas**

- **Proyectando la base de datos**

  - Presentación
  - Preparando el ambiente
  - Preparando el ambiente
  - Creando la base de datos # 1
  - Comando para crear la base de datos
  - Creando la base de datos # 2
  - Comando para crear las tablas
  - Insertando registros # 1
  - Insertar datos a las tablas
  - Insertando registros # 2
  - Insertar datos en lote
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Función aleatorio**

  - Proyecto del aula anterior
  - Función RAND
  - La función RAND de MySQL
  - Generando un número aleatorio
  - Números aleatorios
  - Función número aleatorio
  - Creando funciones en MySQL
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Función cliente aleatorio**

  - Proyecto del aula anterior
  - Función cliente aleatorio # 1
  - Obteniendo un número entero de forma aleatoria
  - Utilizando LIMIT
  - Limitando el output
  - Función cliente aleatorio # 2
  - Creando las funciones para producto y vendedor aleatorio
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Generando ventas y problema con PK**

  - Proyecto del aula anterior
  - Incluyendo venta # 1
  - Funciones y Stored Procedures
  - Incluyendo venta # 2
  - Ciclos iterativos
  - Solucionando problema de PK
  - La clave primaria
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Stored Procedures y TRIGGERS**

  - Proyecto del aula anterior
  - Consultando la facturación
  - Formato de facturación
  - Mejorando TRIGGERS
  - Aplicando Stored Procedures a TRIGGERS
  - Haga lo que hicimos en aula
  - Proyecto final
  - Lo que aprendimos
  - Conclusión
