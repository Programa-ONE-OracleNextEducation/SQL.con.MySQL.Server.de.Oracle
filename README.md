# **SQL con MySQL Server de Oracle**

## **SQL**

La base de datos MySQL es utilizada en miles de aplicaciones web, como por ejemplo, Facebook, Twitter y muchas otras. Aprende SQL, utilizando la base más popular y conozca como acceder a tus datos con confiabilidad, facilidad y alto desempeño.

## **De quien vas a aprender**

- Álvaro Hernando Camacho Diaz

## **Paso a paso**

### **1. Consultas inteligentes para insights valiosos**

Instalarás y aprenderás a trabajar con SQL, utilizando MySQL. Irás convertir y formatear datos y generar relatórios con las principales funciones de SQL.

Con análisis en tiempo real, podrás obtener informaciones transformadoras para tu negócio o para la empresa donde trabajas.

[¿Qué es SQL?](https://www.aluracursos.com/blog/que-es-sql "Alura LATAM").

[Introducción a SQL con MySQL: Manipule y consulte datos](https://app.aluracursos.com/course/introduccion-sql-mysql-manipule-consulte-datos "Introducción a SQL con MySQL: Manipule y consulte datos"). Contenido [aqui](./Docs/introducción_a_sql_con_mysql_manipule_y_consulte_datos.md).

[Consultas SQL: Avanzando en SQL con MySQL](https://app.aluracursos.com/course/consultas-sql-mysql "Consultas SQL: Avanzando en SQL con MySQL"). Contenido [aqui](./Docs/consultas_sql_avanzando_en_sql_con_mysql.md).

[SELECT, INSERT, UPDATE y DELETE en SQL: aprende a utilizar cada uno](https://www.aluracursos.com/blog/select-insert-update-delete-sql "Alura LATAM").

[En SQL, null es null, vacío está vacío](https://www.aluracursos.com/blog/en-sql-null-es-null-vacio-es-vacio "Alura LATAM").

[¿Qué es SQL y NoSQL?](https://www.youtube.com/watch?v=cLLKVd5CNLc&t=630s "YouTube").

### **2. Avanzado en manipulación de datos**

Ahora, irás profundizar en la creación de relatórios significativos. Desarrollarás tablas con llaves primárias y externas, entenderás el commit y rollback, auto-incremento, patrones y triggers.

Después, será el momento de hacer el tratamento de errores de manera correcta y encapsular el código en funciones y stored procedures.

Aprenderás a utilizar los comandos DML, diversas funciones que irán facilitar tu trabajo en el momento de obtener datos, utilizar patrones y triggers.

[Comandos DML: Manipulación de datos con MySQL](https://app.aluracursos.com/course/comandos-dml-manipulacion-datos-mysql "Comandos DML: Manipulación de datos con MySQL"). Contenido [aqui](./Docs/comandos_dml_manipulacion_datos_con_mysql.md).

[Procedures SQL: Ejecutando código en MySQL](https://app.aluracursos.com/course/procedures-sql-codigo-mysql "Procedures SQL: Ejecutando código en MySQL"). Contenido [aqui](./Docs/procedures_sql_ejecutando_codigo_en_mysql.md).

[SQL con MySQL: Proyecto final](https://app.aluracursos.com/course/sql-mysql-proyecto-final "SQL con MySQL: Proyecto final"). Contenido [aqui](./Docs/sql_con_mysql_proyecto_final.md).

### **3. Administrando MySQL Server**

En esta última parte conocerás como es el trabajo diário de un/una profesional responsable por gerenciar, instalar, configurar, actualizar y monitorar una base de datos: un/una **DBA** (_DataBase Administrator_).

Conocerás el plano de ejecución y sus características, indicadores y joins. Aprenderás a manejar backups y entenderás que es y como funciona la recuperación de datos.

[Administración de MySQL: Seguridad y optimización de la base de datos - Parte 1](https://app.aluracursos.com/course/mysql-seguridad-optmizacion-base-datos-parte1 "Administración de MySQL: Seguridad y optimización de la base de datos - Parte 1"). Contenido [aqui](./Docs/administracion_de_mysql_parte_1.md).

[Administración de MySQL: Seguridad y optimización de la base de datos - Parte 2](https://app.aluracursos.com/course/mysql-seguridad-optmizacion-base-datos-parte2 "Administración de MySQL: Seguridad y optimización de la base de datos - Parte 2"). Contenido [aqui](./Docs/administracion_de_mysql_parte_2.md).
